/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
//========= BSP ==============
#include "stm32f4_discovery.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
typedef enum eMode {POST, IDLE, SETTING, RUNNING, WAITING, ALARM, FAILSAFE} Mode;

typedef enum eEvent {EV_NONE, EV_OUTPUT_ERR, EV_COMMU_ERR, EV_SENSOR_ERR} Event;

typedef struct {
	Mode output;
	Mode commu;
	Mode sensor;
        Mode tracking;
} evState_t;

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
extern volatile Mode currentMode;
extern int errno;
/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
extern int __io_putchar(int ch) __attribute__((weak));
extern int __io_getchar(void) __attribute__((weak));
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define MB3_RST_Pin GPIO_PIN_4
#define MB3_RST_GPIO_Port GPIOE
#define MB3_CS_Pin GPIO_PIN_5
#define MB3_CS_GPIO_Port GPIOE
#define MB1_AN_Pin GPIO_PIN_1
#define MB1_AN_GPIO_Port GPIOC
#define MB2_AN_Pin GPIO_PIN_2
#define MB2_AN_GPIO_Port GPIOC
#define MB2_AN_EXTI_IRQn EXTI2_IRQn
#define USER_BTN_Pin GPIO_PIN_0
#define USER_BTN_GPIO_Port GPIOA
#define USER_BTN_EXTI_IRQn EXTI0_IRQn
#define MB1_PWM_Pin GPIO_PIN_1
#define MB1_PWM_GPIO_Port GPIOA
#define DBG_TX_Pin GPIO_PIN_2
#define DBG_TX_GPIO_Port GPIOA
#define DBG_RX_Pin GPIO_PIN_3
#define DBG_RX_GPIO_Port GPIOA
#define MB3_AN_Pin GPIO_PIN_5
#define MB3_AN_GPIO_Port GPIOC
#define MB3_PWM_Pin GPIO_PIN_1
#define MB3_PWM_GPIO_Port GPIOB
#define MB3_INT_Pin GPIO_PIN_2
#define MB3_INT_GPIO_Port GPIOD
#define MB3_INT_EXTI_IRQn EXTI2_IRQn
#define MB1_INT_Pin GPIO_PIN_7
#define MB1_INT_GPIO_Port GPIOE
#define MB1_INT_EXTI_IRQn EXTI9_5_IRQn
#define MB1_RST_Pin GPIO_PIN_10
#define MB1_RST_GPIO_Port GPIOE
#define MB2_CS_Pin GPIO_PIN_14
#define MB2_CS_GPIO_Port GPIOE
#define MB12_TX_Pin GPIO_PIN_10
#define MB12_TX_GPIO_Port GPIOB
#define MB12_RX_Pin GPIO_PIN_11
#define MB12_RX_GPIO_Port GPIOB
#define MB1_CS_Pin GPIO_PIN_12
#define MB1_CS_GPIO_Port GPIOB
#define MB12_SCK_Pin GPIO_PIN_13
#define MB12_SCK_GPIO_Port GPIOB
#define MB12_MISO_Pin GPIO_PIN_14
#define MB12_MISO_GPIO_Port GPIOB
#define MB12_MOSI_Pin GPIO_PIN_15
#define MB12_MOSI_GPIO_Port GPIOB
#define LD4_Pin GPIO_PIN_12
#define LD4_GPIO_Port GPIOD
#define LD3_Pin GPIO_PIN_13
#define LD3_GPIO_Port GPIOD
#define LD5_Pin GPIO_PIN_14
#define LD5_GPIO_Port GPIOD
#define LD6_Pin GPIO_PIN_15
#define LD6_GPIO_Port GPIOD
#define MB34_TX_Pin GPIO_PIN_6
#define MB34_TX_GPIO_Port GPIOC
#define MB34_RX_Pin GPIO_PIN_7
#define MB34_RX_GPIO_Port GPIOC
#define MB34_SDA_Pin GPIO_PIN_9
#define MB34_SDA_GPIO_Port GPIOC
#define MB34_SCL_Pin GPIO_PIN_8
#define MB34_SCL_GPIO_Port GPIOA
#define MB4_RST_Pin GPIO_PIN_6
#define MB4_RST_GPIO_Port GPIOD
#define MB4_CS_Pin GPIO_PIN_7
#define MB4_CS_GPIO_Port GPIOD
#define MB4_INT_Pin GPIO_PIN_11
#define MB4_INT_GPIO_Port GPIOC
#define MB4_INT_EXTI_IRQn EXTI15_10_IRQn
#define MB34_SCK_Pin GPIO_PIN_3
#define MB34_SCK_GPIO_Port GPIOB
#define MB34_MISO_Pin GPIO_PIN_4
#define MB34_MISO_GPIO_Port GPIOB
#define MB34_MOSI_Pin GPIO_PIN_5
#define MB34_MOSI_GPIO_Port GPIOB
#define MB12_SDA_Pin GPIO_PIN_7
#define MB12_SDA_GPIO_Port GPIOB
#define MB12_SCL_Pin GPIO_PIN_8
#define MB12_SCL_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

#define USE_SENSOR                              1U
//#define USE_GPS                                 1U
#if defined( USE_SENSOR )
//#define USE_MB3_ADC                             1U
//#define USE_ADC_SENSOR                          1U
#if defined (USE_ADC_SENSOR)
//#define USE_ADC_SENSOR_I2C                      1U
//#define USE_ADC_SENSOR_CT                       1U
#define USE_ADC_SENSOR_4_20MA                   1U
#endif
#define USE_MEMS                                 1U
#ifdef USE_MEMS
#define USE_IMU_I2C                                 1U
#ifndef USE_IMU_I2C
#define USE_IMU_SPI                                 1U
#else
#endif
#endif

//#define USE_ULTRASONIC_SENSOR                   1U
//#define USE_ENCODER                             1U
//#define USE_AQI_SENSOR                          1U

#if defined ( USE_ENCODER )
#define USE_ROTARY_ENCODER                    1U
#endif

#if defined ( USE_ULTRASONIC_SENSOR )
#define USE_PGA460                1U
#define USE_MB7389                1U
#endif

#ifdef USE_AQI_SENSOR
#define USE_PM_SENSOR                           1U
#ifdef USE_PM_SENSOR

#endif /*  USE_PM_SENSOR */

#define USE_SOUND_LVL_SENSOR                    1U
#ifdef USE_SOUND_LVL_SENSOR

#endif /*  USE_SOUND_LVL_SENSOR */

#define USE_CO2_SENSOR                          1U
#ifdef USE_CO2_SENSOR

#define CO2_EN_GPIO_Port    GPIOE
#define CO2_EN_Pin          GPIO_PIN_6

#endif /* USE_CO2_SENSOR */
#endif /* USE_AQI_SENSOR */
#endif /* USE_SENSOR */

#define USE_OUTPUT                              1U
#if defined ( USE_OUTPUT )
//#define USE_BEEP                                1U
#define USE_INDICATOR                           1U


#endif /* USE_OUTPUT */


typedef struct user_data_t {
#ifdef  USE_ADC_SENSOR
	uint16_t ac_current_value;
	uint16_t ac_current_adc_value;
#elif defined ( USE_IMU_I2C )
        
        int16_t ax;
        int16_t ay;
        int16_t az;
        int16_t ts;
        int16_t gx;
        int16_t gy;
        int16_t gz;

#else
	uint16_t io_value;
#endif
	uint16_t temperature_adc_value;
	uint16_t vref_adc_value;

#ifdef USE_HDC1080
	uint16_t amb_temperature_value;
	uint16_t amb_humidity_value;
#endif
	uint16_t period;
} user_data_t;

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
