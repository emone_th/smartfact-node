/*
 * adc.h
 *
 *  Created on: Jun 3, 2020
 *      Author: anolp
 */

#ifndef __ADC_H_
#define __ADC_H_

#include "stdint.h"

typedef struct {
  uint16_t ac_current_value;
  uint16_t ac_current_adc_value;
} ADC_DATA_t;

void ADC_Initialize(void *handler, void *period);
void ADC_StartRead(void const *argument);

#endif /* __ADC_H_ */
