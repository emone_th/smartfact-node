/*
 * ultrasonic_config.h
 *
 *  Created on: Jan 9, 2020
 *      Author: anolp
 */
#ifndef _ULTRASONIC2_T_
#define _ULTRASONIC2_T_

#include "stdbool.h"
#include "stdint.h"

#define T_ULTRASONIC2_P const uint8_t *

typedef struct {
  float distance;
  uint16_t width;
  uint8_t peak;
} ULTRASONIC_t;

typedef struct {
  float diagnosticsNoise;
  float diagnosticsTemperature;
  float diagnosticsFreq;
  float diagnosticsPeriod;
} ULTRASONIC_DIAG_t;

typedef  struct {
  uint16_t err_t;
  ULTRASONIC_t* obj;
  uint8_t DETECT_OBJECT;
  ULTRASONIC_DIAG_t diag;
  uint16_t fw_ver;
  uint32_t sensor_id;
} ULTRASONIC_OBJ_t;

typedef enum {
    _ULTRASONIC2_BAUDRATE_9600,

    _ULTRASONIC2_BAUDRATE_19200,

    _ULTRASONIC2_BAUDRATE_38400,

    _ULTRASONIC2_BAUDRATE_57600,

    _ULTRASONIC2_BAUDRATE_74800,

    _ULTRASONIC2_BAUDRATE_115200,
} ULTRASONIC2_BAUDRATE;

typedef enum {
    _ULTRASONIC2_MIN_RANGE = ((uint16_t)100),

    _ULTRASONIC2_MAX_RANGE = ((uint16_t)8000),
} ULTRASONIC2_RANGE;

typedef enum {
    _ULTRASONIC2_UART_ADDR_UPDATE_0,

    _ULTRASONIC2_UART_ADDR_UPDATE_1,

    _ULTRASONIC2_UART_ADDR_UPDATE_2,

    _ULTRASONIC2_UART_ADDR_UPDATE_3,

    _ULTRASONIC2_UART_ADDR_UPDATE_4,

    _ULTRASONIC2_UART_ADDR_UPDATE_5,

    _ULTRASONIC2_UART_ADDR_UPDATE_6,

    _ULTRASONIC2_UART_ADDR_UPDATE_7,
} ULTRASONIC2_UART_ADDR;

typedef enum {
    _ULTRASONIC2_TVG_GAIN_RANGE_32_TO_64db,

    _ULTRASONIC2_TVG_GAIN_RANGE_46_TO_78db,

    _ULTRASONIC2_TVG_GAIN_RANGE_52_TO_84db,

    _ULTRASONIC2_TVG_GAIN_RANGE_58_TO_90db,
} ULTRASONIC2_TVG_GAIN_RANGE;

typedef enum {
    _ULTRASONIC2_TVG_LEVEL_25_PERCENT,

    _ULTRASONIC2_TVG_LEVEL_50_PERCENT,

    _ULTRASONIC2_TVG_LEVEL_75_PERCENT,

    _ULTRASONIC2_TVG_LEVEL_CUSTOM,

} ULTRASONIC2_TVG_LEVEL;

typedef enum {
    _ULTRASONIC2_ECHO_DATA_DUMP_OF_PRESET_1,
    _ULTRASONIC2_ECHO_DATA_DUMP_OF_PRESET_2,
} ULTRASONIC2_ECHO_DATA_DUMP;

typedef enum {
    _ULTRASONIC2_DETECT_OBJECT_1 = 1,

    _ULTRASONIC2_DETECT_OBJECT_2,

    _ULTRASONIC2_DETECT_OBJECT_3,

    _ULTRASONIC2_DETECT_OBJECT_4,

    _ULTRASONIC2_DETECT_OBJECT_5,

    _ULTRASONIC2_DETECT_OBJECT_6,

    _ULTRASONIC2_DETECT_OBJECT_7,

    _ULTRASONIC2_DETECT_OBJECT_8,
} ULTRASONIC2_DETECT;

typedef enum {
    _ULTRASONIC2_DEVICE_DISABLE,

    _ULTRASONIC2_DEVICE_ENABLE,
} ULTRASONIC2_DEVICE;

void ultrasonic_systemInit(void);
void ultrasonic_applicationInit(void *obj);
void ultrasonic_applicationTask(bool range);

void Ultrasonic_RX_ISR(void);

void ultrasonic2_uartDriverInit(T_ULTRASONIC2_P gpioObj,
    T_ULTRASONIC2_P uartObj);

void ultrasonic2_getc(uint8_t input);
//Function is used to store responses. More...

void ultrasonic2_enable(uint8_t state);
//  Function for enable chip. More...

void ultrasonic2_setAmbientTemperature(int8_t temp);
//  Function for sets ambient temperature. More...

void ultrasonic2_configuration(uint8_t echo);
//  Function for configuration chip. More...

void ultrasonic2_setComunication(uint8_t uartAddrUpdate);
//  Function for init comunication. More...

void ultrasonic2_defaultConfiguration(void);
//  Function for init device. More...

void ultrasonic2_setThresholds(uint8_t *TH_P1, uint8_t *TH_P2);
//  Function for init thresholds. More...

void ultrasonic2_setTVG(uint8_t agr, uint8_t *TVG);
//  Function for init TVG. More...

void ultrasonic2_sendCommand(uint8_t cmd, uint8_t numObjUpdate);
//  Function for send command. More...

void ultrasonic2_startUltrasonicMeasurement(bool range, uint8_t detectObj);
//  Function for start ultrasonic measurement. More...

void ultrasonic2_getUltrasonicMeasurement(uint8_t numObj, float *distOUT,
    uint16_t *widthOUT, uint8_t *ampOUT);
//  Function for get ultrasonic measurement. More...

void ultrasonic2_getDiagnostics(float *freqOUT, float *periodOUT,
    float *tempOUT, float *noiseOUT);
//  Function for read diagnostics. More...
#endif