/**
  ******************************************************************************
  * @file           : beep.h
  * @brief          : Header for beep.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 EmOne.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by EmOne under Ultimate Liberty license
  * GPL, the "License"; You may not use this file except in compliance with
  * the License.
  *
  ******************************************************************************
  */
#include <stdbool.h>
#include <stdint.h>
#include "main.h"

#ifdef USE_BEEP

#define PCLK 84000000
//#define cS  261
//#define dS  294
//#define e   329
//#define fS  349
//#define g   391
//#define gS  415
//#define aHa 440
//#define aS  455
//#define b   466
//#define cH  523
//#define cSH 554
//#define dH  587
//#define dSH 622
//#define eH  659
//#define fH  698
//#define fSH 740
//#define gH  784
//#define gSH 830
//#define aH  880

// Single tone definition
typedef struct {
	uint16_t frequency;
	uint8_t  duration;
} Tone_TypeDef;


static const Tone_TypeDef tones_startup[] = {
		{2000,3},
		{   0,3},
		{3000,3},
		{   0,3},
		{4000,3},
		{   0,3},
		{1200,4},
		{   0,6},
		{4500,6},
		{   0,0}     // <-- tones end
};

static const Tone_TypeDef tones_3beep[] = {
		{4000, 3},
		{   0,10},
		{1000, 6},
		{   0,10},
		{4000, 3},
		{   0, 0}
};

// "Super Mario bros." =)
static const Tone_TypeDef tones_SMB[] = {
		{2637,18}, // E7 x2
		{   0, 9}, // x3
		{2637, 9}, // E7
		{   0, 9}, // x3
		{2093, 9}, // C7
		{2637, 9}, // E7
		{   0, 9}, // x3
		{3136, 9}, // G7
		{   0,27}, // x3
		{1586, 9}, // G6
		{   0,27}, // x3

		{2093, 9}, // C7
		{   0,18}, // x2
		{1586, 9}, // G6
		{   0,18}, // x2
		{1319, 9}, // E6
		{   0,18}, // x2
		{1760, 9}, // A6
		{   0, 9}, // x1
		{1976, 9}, // B6
		{   0, 9}, // x1
		{1865, 9}, // AS6
		{1760, 9}, // A6
		{   0, 9}, // x1

		{1586,12}, // G6
		{2637,12}, // E7
		{3136,12}, // G7
		{3520, 9}, // A7
		{   0, 9}, // x1
		{2794, 9}, // F7
		{3136, 9}, // G7
		{   0, 9}, // x1
		{2637, 9}, // E7
		{   0, 9}, // x1
		{2093, 9}, // C7
		{2349, 9}, // D7
		{1976, 9}, // B6
		{   0,18}, // x2

		{2093, 9}, // C7
		{   0,18}, // x2
		{1586, 9}, // G6
		{   0,18}, // x2
		{1319, 9}, // E6
		{   0,18}, // x2
		{1760, 9}, // A6
		{   0, 9}, // x1
		{1976, 9}, // B6
		{   0, 9}, // x1
		{1865, 9}, // AS6
		{1760, 9}, // A6
		{   0, 9}, // x1

		{1586,12}, // G6
		{2637,12}, // E7
		{3136,12}, // G7
		{3520, 9}, // A7
		{   0, 9}, // x1
		{2794, 9}, // F7
		{3136, 9}, // G7
		{   0, 9}, // x1
		{2637, 9}, // E7
		{   0, 9}, // x1
		{2093, 9}, // C7
		{2349, 9}, // D7
		{1976, 9}, // B6

		{   0, 0}
};

__IO uint32_t key_note_freq;
__IO       uint32_t          _beep_duration;
__IO       bool              _tones_playing;
__IM const Tone_TypeDef     *_tones;

#define BEEPER_TIM               TIM3
#define BEEPER_TIM_IRQN          TIM3_IRQn

void BEEPER_Init(void *handler);

void BEEPER_Enable(uint16_t freq, uint32_t duration);

void BEEPER_Disable(void);

void BEEPER_PlayTones(const Tone_TypeDef * tones);

void BEEPER_IRQ( void );

#endif /* USE_BEEP */
