/*
 * encoder.h
 *
 *  Created on: Dec 30, 2020
 *      Author: anolp
 */

#ifndef _ENCODER_H_
#define _ENCODER_H_

#include "stdint.h"
#include "stdbool.h"
#include "Click_Rotary_Y_types.h"
#include "Click_Rotary_Y_config.h"

typedef struct {

} rotary_cfg_t; 

void encoder_system_init( void );
void encoder_application_init ( void );
void encoder_application_task ( void );

#endif /* _ENCODER_H_ */