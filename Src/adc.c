/*
 * adc.c
 *
 *  Created on: Jun 3, 2020
 *      Author: anolp
 */
#include "adc.h"
#include "cmsis_os.h"
#include "main.h"

extern osMailQId msgMailHandle;
extern user_data_t sData_t;
osTimerId ac_current_read_TimerHandle;

#if defined ( USE_ADC_SENSOR_CT ) 
ADC_HandleTypeDef *hadc;
#ifdef USE_MB1_ADC
extern SPI_HandleTypeDef hspi2;
#elif defined(USE_MB3_ADC)
extern SPI_HandleTypeDef hspi1;
#endif
#elif defined(USE_ADC_SENSOR_I2C)
//ADDR PIN CONNECTION SLAVE ADDRESS
//GND 1001000
//VDD 1001001
//SDA 1001010
//SCL 1001011
#define I2C_SLAVE_ADDR 0x48

I2C_HandleTypeDef *hi2c;
static void I2C_Read(uint8_t regAddr, uint8_t *val, size_t len);
static void I2C_Write(uint8_t regAddr, uint8_t *val, size_t len);
#define CONVERSION_REG_ADDR 0
#define CONFIG_REG_ADDR 1
#define LO_THRES_REG_ADDR 2
#define HI_THRES_REG_ADDR 3
uint16_t config = 0;
uint16_t conversion = 0;
#elif defined ( USE_ADC_SENSOR_4_20MA )
extern SPI_HandleTypeDef hspi1;
uint16_t valueADCOld = 0;
uint16_t sensitivity = 10;

#endif

#ifdef USE_ADC_SENSOR
void ADC_Initialize(void *handler, void *period) {
  if (handler != NULL) {
#ifdef USE_ADC_SENSOR_CT
    hadc = (ADC_HandleTypeDef *)handler;
#ifdef USE_MB1_ADC
    HAL_GPIO_WritePin(MB1_CS_GPIO_Port, MB1_CS_Pin, GPIO_PIN_SET);
#elif defined(USE_MB3_ADC)
    HAL_GPIO_WritePin(MB3_CS_GPIO_Port, MB3_CS_Pin, GPIO_PIN_SET);
#endif
#elif defined(USE_ADC_SENSOR_I2C)
    hi2c = (I2C_HandleTypeDef *)handler;
    I2C_Read(CONFIG_REG_ADDR, (uint8_t *)&config, sizeof(config));
    config = 0x72;
    config |= 0x83 << 8;
    I2C_Write(CONFIG_REG_ADDR, (uint8_t *)&config, sizeof(config));
    I2C_Read(CONFIG_REG_ADDR, (uint8_t *)&config, sizeof(config));
    //		buf[0] = 0xB2;
    //		I2C_Write(CONFIG_REG_ADDR, buf, sizeof(buf));
    //		buf[0] = 0xC2;
    //		I2C_Write(CONFIG_REG_ADDR, buf, sizeof(buf));
    //		buf[0] = 0xD2;
    //		I2C_Write(CONFIG_REG_ADDR, buf, sizeof(buf));
#elif defined ( USE_ADC_SENSOR_4_20MA )
  HAL_GPIO_WritePin(MB3_CS_GPIO_Port, MB3_CS_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(MB3_INT_GPIO_Port, MB3_INT_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(MB3_INT_GPIO_Port, MB3_INT_Pin, GPIO_PIN_SET);
  HAL_Delay(100);
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES_RXONLY;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_4;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
#endif
  }

  osTimerDef(ac_current_read_Timer, ADC_StartRead);
  ac_current_read_TimerHandle = osTimerCreate(osTimer(ac_current_read_Timer),
      osTimerPeriodic, NULL);
  osTimerStart(ac_current_read_TimerHandle, 1000 * 5);
}

void ADC_StartRead(void const *argument) {

#ifdef USE_ADC_SENSOR_CT
  ADC_ChannelConfTypeDef ch;

  osTimerStop(ac_current_read_TimerHandle);
  //TODO: read ac current



#ifdef USE_MB1_ADC
  HAL_GPIO_WritePin(MB1_CS_GPIO_Port, MB1_CS_Pin, GPIO_PIN_RESET);
  HAL_SPI_Receive(&hspi2, (uint8_t*) &sData_t.ac_current_value, 2, 500);
  HAL_GPIO_WritePin(MB1_CS_GPIO_Port, MB1_CS_Pin, GPIO_PIN_SET);

  ch.Channel = ADC_CHANNEL_11;
#elif defined(USE_MB3_ADC)
  HAL_GPIO_WritePin(MB3_CS_GPIO_Port, MB3_CS_Pin, GPIO_PIN_RESET);
  HAL_SPI_Receive(&hspi1, (uint8_t*) &sData_t.ac_current_value, 2, 500);
  HAL_GPIO_WritePin(MB3_CS_GPIO_Port, MB3_CS_Pin, GPIO_PIN_SET);

  ch.Channel = ADC_CHANNEL_15;
#endif
  ch.Rank = 1;
  ch.SamplingTime = ADC_SAMPLETIME_144CYCLES;
  ch.Offset = 0;
  HAL_ADC_ConfigChannel(hadc, &ch);
  HAL_ADC_Start(hadc);
  HAL_ADC_PollForConversion(hadc, 500);
  sData_t.ac_current_adc_value = HAL_ADC_GetValue(hadc);
  HAL_ADC_Stop(hadc);

  ch.Channel = ADC_CHANNEL_18; //TEMPERATURE
  ch.SamplingTime = ADC_SAMPLETIME_112CYCLES;
  HAL_ADC_ConfigChannel(hadc, &ch);
  HAL_ADC_Start(hadc);
  HAL_ADC_PollForConversion(hadc, 500);
  sData_t.temperature_adc_value = HAL_ADC_GetValue(hadc);
  HAL_ADC_Stop(hadc);

  ch.Channel = ADC_CHANNEL_VREFINT;
  ch.SamplingTime = ADC_SAMPLETIME_112CYCLES;
  HAL_ADC_ConfigChannel(hadc, &ch);
  HAL_ADC_Start(hadc);
  HAL_ADC_PollForConversion(hadc, 500);
  sData_t.vref_adc_value = HAL_ADC_GetValue(hadc);
  HAL_ADC_Stop(hadc);

  sData_t.vref_adc_value =
      (uint16_t)((1226 * 4095) / sData_t.vref_adc_value);
  //  sData_t.temperature_adc_value = COMPUTE_TEMPERATURE(sData_t.temperature_adc_value);

  //	  hdc1080_tsensor_drv.ReadTempHumi(0,
  //	                      &te,
  //	                      &hu);
  //
  //	  sData_t.amb_temperature_value=((((te/65536.0)*165.0)-40.0) * 10.0);
  //	  sData_t.amb_humidity_value=((hu/65536.0)*1000.0);

#elif defined(USE_ADC_SENSOR_I2C)

  I2C_Read(CONFIG_REG_ADDR, (uint8_t *)&config, sizeof(config));
  I2C_Read(CONVERSION_REG_ADDR, (uint8_t *)&conversion, sizeof(conversion));
  sData_t.ac_current_adc_value = conversion >> 8;
  sData_t.ac_current_adc_value |= (conversion & 0xFF) << 8;
#elif defined ( USE_ADC_SENSOR_4_20MA )
  uint8_t tmp[2];
  HAL_GPIO_WritePin(MB3_CS_GPIO_Port, MB3_CS_Pin, GPIO_PIN_RESET);
  HAL_SPI_Receive(&hspi1, (uint8_t*) tmp, 2, 1000);
  HAL_GPIO_WritePin(MB3_CS_GPIO_Port, MB3_CS_Pin, GPIO_PIN_SET);
  sData_t.ac_current_value = ((tmp[1] << 8) | tmp[0]) & 0x0fff;
  //HAL_GPIO_WritePin(MB3_INT_GPIO_Port, MB3_INT_Pin, GPIO_PIN_RESET);

  if ( ( ( sData_t.ac_current_value - valueADCOld ) > sensitivity ) && ( ( valueADCOld - sData_t.ac_current_value ) > sensitivity ) )
    {
        //mikrobus_logWrite( " Input ADC: ", _LOG_TEXT );
        //FloatToStr( inputADC, logText );
        //mikrobus_logWrite( logText, _LOG_TEXT );
        //mikrobus_logWrite( " mA", _LOG_LINE );
        //mikrobus_logWrite("------------------------", _LOG_LINE);

        valueADCOld = sData_t.ac_current_value;
 
     }

#endif

  printf("V:0x%04X ", sData_t.vref_adc_value);
  printf("T:0x%04X ", sData_t.temperature_adc_value);
#ifdef  USE_ADC_SENSOR
  printf("A:0x%04X ", sData_t.ac_current_adc_value);
  printf("C:0x%04X ", sData_t.ac_current_value);
#else
  printf("I:0x%04X ", sData_t.io_value);
#endif

#ifdef USE_HDC1080
  printf("AT:0x%04X ", sData_t.amb_temperature_value);
  printf("AH:0x%04X ",sData_t.amb_humidity_value);
#endif
  printf("P:%d\r\n", sData_t.period);

//  osMailPut(msgMailHandle, &sData_t);

//  osTimerStart(ac_current_read_TimerHandle, 1000 * 5 * sData_t.period);
  osTimerStart(ac_current_read_TimerHandle, 1000);
}

#ifdef USE_ADC_SENSOR_I2C
static void I2C_Read(uint8_t regAddr, uint8_t *val, size_t len) {
  HAL_StatusTypeDef ret;
  uint16_t slave_id = I2C_SLAVE_ADDR << 1;
  //Wake up
  ret = HAL_I2C_IsDeviceReady(hi2c, slave_id, 1, 25);

  ret = HAL_I2C_Master_Transmit(hi2c, slave_id, &regAddr, 1, 500);

  ret = HAL_I2C_Master_Receive(hi2c, slave_id, val, len, 500);
  if (ret == HAL_OK) {
    //Read registor
    //		printf("CO2 comm. ok\r\n");
  } else {
    printf("ADC comm. err\r\n");
  }
}

static void I2C_Write(uint8_t regAddr, uint8_t *val, size_t len) {
  HAL_StatusTypeDef ret;
  uint16_t slave_id = I2C_SLAVE_ADDR << 1;
  //Wake up
  ret = HAL_I2C_IsDeviceReady(hi2c, slave_id, 1, 25);

  ret = HAL_I2C_Mem_Write(hi2c, slave_id, regAddr, I2C_MEMADD_SIZE_8BIT, val, len, 500);

  if (ret == HAL_OK) { //Write reg. address val
    printf("ADC comm. ok\r\n");
  } else {
    printf("ADC comm. err\r\n");
  }
}
#endif /* (USE_ADC_SENSER_I2C) */
#endif /* USE_ADC_SENSOR */