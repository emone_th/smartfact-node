/*
 * adc.c
 *
 *  Created on: Jun 3, 2020
 *      Author: anolp
 */
#include "imu.h"
#include "cmsis_os.h"
#include "main.h"
#include "iam20680/iam20680.h"

extern osMailQId msgMailHandle;
extern user_data_t sData_t;

#if defined(USE_IMU_I2C)

osTimerId imu_read_TimerHandle;
/**
  * @brief  IMU Interface pins
  */
#define USE_MB12
//#define USE_MB34

#if defined ( USE_MB12 )
#define IMU_I2Cx                            I2C1
#define IMU_I2Cx_CLK_ENABLE()               __HAL_RCC_I2C1_CLK_ENABLE()
#define IMU_I2Cx_SCL_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOB_CLK_ENABLE()
#define IMU_I2Cx_SDA_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOB_CLK_ENABLE()
#define IMU_I2Cx_SCL_SDA_AF                 GPIO_AF4_I2C1
#define IMU_I2Cx_SCL_GPIO_PORT          GPIOB
#define IMU_I2Cx_SCL_PIN                    GPIO_PIN_8
#define IMU_I2Cx_SDA_GPIO_PORT          GPIOB
#define IMU_I2Cx_SDA_PIN                    GPIO_PIN_7

#define IMU_I2Cx_FORCE_RESET()              __HAL_RCC_I2C1_FORCE_RESET()
#define IMU_I2Cx_RELEASE_RESET()            __HAL_RCC_I2C1_RELEASE_RESET()

/* I2C interrupt requests */
#define IMU_I2Cx_EV_IRQn                    I2C1_EV_IRQn
#define IMU_I2Cx_ER_IRQn                    I2C1_ER_IRQn

#define IMU_INT_GPIO_PORT                 GPIOE                      /* GPIOE */
#define IMU_INT_GPIO_CLK_ENABLE()         __HAL_RCC_GPIOE_CLK_ENABLE()
#define IMU_INT_GPIO_CLK_DISABLE()        __HAL_RCC_GPIOE_CLK_DISABLE()
#define IMU_INT_PIN                      GPIO_PIN_7                 /* PE.07 */
#define IMU_INT_EXTI_IRQn                EXTI9_5_IRQn

#define IMU_SYNC_GPIO_PORT                 GPIOA                      /* GPIOA */
#define IMU_SYNC_GPIO_CLK_ENABLE()         __HAL_RCC_GPIOA_CLK_ENABLE()
#define IMU_SYNC_GPIO_CLK_DISABLE()        __HAL_RCC_GPIOA_CLK_DISABLE()
#define IMU_SYNC_PIN                      GPIO_PIN_1                 /* PA.01 */
#elif defined ( USE_MB34 )
#define IMU_I2Cx                            I2C3
#define IMU_I2Cx_CLK_ENABLE()               __HAL_RCC_I2C3_CLK_ENABLE()
#define IMU_I2Cx_SCL_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOA_CLK_ENABLE()
#define IMU_I2Cx_SDA_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOC_CLK_ENABLE()
#define IMU_I2Cx_SCL_SDA_AF                 GPIO_AF4_I2C3
#define IMU_I2Cx_SCL_GPIO_PORT          GPIOA
#define IMU_I2Cx_SCL_PIN                    GPIO_PIN_8
#define IMU_I2Cx_SDA_GPIO_PORT          GPIOC
#define IMU_I2Cx_SDA_PIN                    GPIO_PIN_9

#define IMU_I2Cx_FORCE_RESET()              __HAL_RCC_I2C3_FORCE_RESET()
#define IMU_I2Cx_RELEASE_RESET()            __HAL_RCC_I2C3_RELEASE_RESET()

/* I2C interrupt requests */
#define IMU_I2Cx_EV_IRQn                    I2C3_EV_IRQn
#define IMU_I2Cx_ER_IRQn                    I2C3_ER_IRQn

#define IMU_INT_GPIO_PORT                 GPIOD                      /* GPIOD */
#define IMU_INT_GPIO_CLK_ENABLE()         __HAL_RCC_GPIOD_CLK_ENABLE()
#define IMU_INT_GPIO_CLK_DISABLE()        __HAL_RCC_GPIOD_CLK_DISABLE()
#define IMU_INT_PIN                      GPIO_PIN_2                 /* PD.00 */
#define IMU_INT_EXTI_IRQn                EXTI2_IRQn

#define IMU_SYNC_GPIO_PORT                 GPIOB                      /* GPIOB */
#define IMU_SYNC_GPIO_CLK_ENABLE()         __HAL_RCC_GPIOB_CLK_ENABLE()
#define IMU_SYNC_GPIO_CLK_DISABLE()        __HAL_RCC_GPIOB_CLK_DISABLE()
#define IMU_SYNC_PIN                      GPIO_PIN_1                 /* PB.01 */
#else
  error("");
#endif
// ADDR PIN CONNECTION SLAVE ADDRESS
// I2C ADDRESS SA0 = 0 <1101000> SA0 = 1 <1101001>
#define I2C_SLAVE_ADDR 0x69 //SA0 = 1 on default IMU 9 Click board

static IMU_DrvTypeDef * IMU_Drv;

static I2C_HandleTypeDef I2cHandle;
static void I2C_Read(uint8_t regAddr, uint8_t *val, size_t len);
static void I2C_Write(uint8_t regAddr, uint8_t *val, size_t len);

uint16_t config = 0;
uint16_t conversion = 0;
static uint16_t FIFO_COUNT = 0;

void IMU_StartRead(void const *argument);

#endif
void    IMU_IO_Init(void)
{
  if(HAL_I2C_GetState(&I2cHandle) == HAL_I2C_STATE_RESET)
  {
    /* IMU_I2Cx peripheral configuration */
    I2cHandle.Init.ClockSpeed = BSP_I2C_SPEED;
    I2cHandle.Init.DutyCycle = I2C_DUTYCYCLE_2;
    I2cHandle.Init.OwnAddress1 = 0x0;
    I2cHandle.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
    I2cHandle.Instance = IMU_I2Cx;

    /* Init the I2C */
    GPIO_InitTypeDef  GPIO_InitStruct;

    /* Enable I2C GPIO clocks */
    IMU_I2Cx_SCL_GPIO_CLK_ENABLE();
    IMU_I2Cx_SDA_GPIO_CLK_ENABLE();

    /* IMU_I2Cx SCL and SDA pins configuration ---------------------------*/
    GPIO_InitStruct.Pin = IMU_I2Cx_SCL_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
    GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Alternate  = IMU_I2Cx_SCL_SDA_AF;
    HAL_GPIO_Init(IMU_I2Cx_SCL_GPIO_PORT, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = IMU_I2Cx_SDA_PIN;
    HAL_GPIO_Init(IMU_I2Cx_SDA_GPIO_PORT, &GPIO_InitStruct);

    /* Enable the IMU_I2Cx peripheral clock */
    IMU_I2Cx_CLK_ENABLE();

    /* Force the I2C peripheral clock reset */
    IMU_I2Cx_FORCE_RESET();

    /* Release the I2C peripheral clock reset */
    IMU_I2Cx_RELEASE_RESET();

    if(HAL_I2C_Init(&I2cHandle) != HAL_OK)
    { 
      Error_Handler();
    }

    /* Enable and set I2Cx Interrupt to the highest priority */
    HAL_NVIC_SetPriority(IMU_I2Cx_EV_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(IMU_I2Cx_EV_IRQn);

    /* Enable and set I2Cx Interrupt to the highest priority */
    HAL_NVIC_SetPriority(IMU_I2Cx_ER_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(IMU_I2Cx_ER_IRQn);
  }
}

void IMU_IO_ITConfig(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  /* Enable INT2 GPIO clock and configure GPIO PINs to detect Interrupts */
  IMU_INT_GPIO_CLK_ENABLE();

  /* Configure GPIO PINs to detect Interrupts */
  GPIO_InitStructure.Pin = IMU_INT_PIN;
  GPIO_InitStructure.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStructure.Speed = GPIO_SPEED_FAST;
  GPIO_InitStructure.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(IMU_INT_GPIO_PORT, &GPIO_InitStructure);

  /* Enable and set IMU meter INT2 to the lowest priority */
  HAL_NVIC_SetPriority((IRQn_Type)IMU_INT_EXTI_IRQn, 0x0F, 0);
  HAL_NVIC_EnableIRQ((IRQn_Type)IMU_INT_EXTI_IRQn);
}

void    IMU_IO_Write(uint8_t* pBuffer, uint8_t WriteAddr, uint16_t NumByteToWrite)
{
  I2C_Write(WriteAddr, pBuffer, NumByteToWrite);
}

void    IMU_IO_Read(uint8_t* pBuffer, uint8_t ReadAddr, uint16_t NumByteToRead)
{
  I2C_Read(ReadAddr, pBuffer, NumByteToRead);
}

uint8_t IMU_Initialize(void) {
  uint8_t ret = HAL_ERROR;
  IAM20680_InitTypeDef init_def;
  IAM20680_InterruptConfigTypeDef irq_def;
  //if (handler != NULL) {
  if(imuDrv.ReadID() == I_AM_IAM20680) {
#if defined(USE_IMU_I2C)
    IMU_Drv = &imuDrv;
  
    //init_def.Axes_Enable = BIT_FIFO_LP_EN | BIT_PWR_ACCEL_STBY | BIT_PWR_GYRO_STBY;
    //init_def.Full_Scale = 1;
    //init_def.Filter_BW = 1;
    //init_def.Output_DataRate = 1;
    //init_def.SPI_Wire = 0;
    //init_def.Self_Test = 1;

    IMU_Drv->Init( BIT_H_RESET | BIT_CLK_PLL);
    
    //irq_def.Interrupt_Selection_Enable = 1; //
    //irq_def.Interrupt_Request = 1; //

    IMU_Drv->ConfigIT(BIT_DATA_RDY_EN);
    //IMU_Drv->EnableIT();
    
    ret = HAL_OK;
#endif
  }
  
  osTimerDef(imu_read_Timer, IMU_StartRead);
  imu_read_TimerHandle = osTimerCreate(osTimer(imu_read_Timer),
      osTimerPeriodic, NULL);
  osTimerStart(imu_read_TimerHandle, 1000);

  return ret;
}

void IMU_StartRead(void const *argument) {

  //TODO: sync imu
  IMU_Drv->GetIMU(&sData_t);

  printf("AX:0x%04X ", sData_t.ax);
  printf("AY:0x%04X ", sData_t.ay);
  printf("AZ:0x%04X\r\n", sData_t.az);
  printf("TS:0x%04X\r\n", sData_t.ts);
  printf("GX:0x%04X ", sData_t.gx);
  printf("GY:0x%04X ", sData_t.gy);
  printf("GZ:0x%04X\r\n", sData_t.gz);
  printf("T:%d\r\n", sData_t.temperature_adc_value);
  printf("S:%d\r\n", sData_t.vref_adc_value);
  printf("P:%d\r\n", sData_t.period);

//  osMailPut(msgMailHandle, &sData_t);
}

void IMU_IRQ_Handler(void *handler) {
  uint8_t status = IMU_Drv->ITStatus(handler);
  
  printf("IMU<REG_INT_STATUS>: ");
  if(status & BIT_WOM_X_INT) {  //WOM on X axis
    printf("BIT_WOM_X_INT occured ");
  }

  if(status & BIT_WOM_Y_INT) {  //WOM on Y axis
    printf("BIT_WOM_Y_INT occured ");
  }

  if(status & BIT_WOM_Z_INT) {  //WOM on Z axis
    printf("BIT_WOM_Z_INT occured ");
  }

  if(status & BIT_OVR_FIFO_INT) { //FIFO Overflow
    printf("BIT_OVR_FIFO_INT occured ");
    //Euler angle / Quaternion transform

    //Apply TKF filter
  }

  if(status & BIT_GDRIVE_INT) { //GDrive interupt
    printf("BIT_GDRIVE_INT occured ");
  }

  if(status & BIT_DATA_RDY) { // Data ready
    printf("BIT_DATA_RDY occured ");
  }
  printf("\r\n");
}

#ifdef USE_IMU_I2C
static void I2C_Read(uint8_t regAddr, uint8_t *val, size_t len) {
  HAL_StatusTypeDef ret = HAL_OK;
  uint16_t slave_id = I2C_SLAVE_ADDR << 1;
  //Wake up
  //if(HAL_I2C_IsDeviceReady(&I2cHandle, slave_id, 1, 25) == HAL_OK){

    //ret = HAL_I2C_Master_Transmit(&I2cHandle, slave_id, &regAddr, 1, 500);

    //ret = HAL_I2C_Master_Receive(&I2cHandle, slave_id, val, len, 500);

    ret = HAL_I2C_Mem_Read(&I2cHandle, slave_id, (uint16_t)regAddr, I2C_MEMADD_SIZE_8BIT, val, len, 500);
  //}
  if (ret == HAL_OK) {  //Read registor
    //printf("IMU read comm. ok\r\n");
  } else {
    printf("IMU read comm. err\r\n");
  }
}

static void I2C_Write(uint8_t regAddr, uint8_t *val, size_t len) {
  HAL_StatusTypeDef ret;
  uint16_t slave_id = I2C_SLAVE_ADDR << 1;
  //Wake up
  //if(HAL_I2C_IsDeviceReady(&I2cHandle, slave_id, 1, 25) == HAL_OK){

    //ret = HAL_I2C_Mem_Write(&I2cHandle, slave_id, regAddr, I2C_MEMADD_SIZE_8BIT, val, len, 500);
    ret = HAL_I2C_Mem_Write(&I2cHandle, slave_id, (uint16_t)regAddr, I2C_MEMADD_SIZE_8BIT, val, len, 500);
  //}
  if (ret == HAL_OK) {  //Write reg. address val
    //printf("IMU write comm. ok\r\n");
  } else {
    printf("IMU write comm. err\r\n");
  }
}
#endif /* (USE_IMU_I2C) */

/* Init af threshold to detect acceleration on MEMS */
/* Typical value: 
      - No  acceleration: X, Y inferior to 100 (positive or negative)
      - Max acceleration: X, Y around 2000 (positive or negative) */
//int16_t ThresholdHigh = 200;
//int16_t ThresholdLow = -200;
/**
  * @brief  Read Acceleration data.
  * @param  None
  * @retval None
  */
void IMU_Read(void)
{
  /* IMUmeter variables */
  //int16_t buffer[3] = {0};
  //int16_t xval, yval = 0x00;
  
  ///* Read IMU */
  //BSP_IMU_GetXYZ(buffer);
  
  //xval = buffer[0];
  //yval = buffer[1];
  
  //if((ABS(xval))>(ABS(yval)))
  //{
  //  if(xval > ThresholdHigh)
  //  { 
  //    /* LED5 On */
  //    BSP_LED_On(LED5);
  //    osDelay(10);
  //  }
  //  else if(xval < ThresholdLow)
  //  { 
  //    /* LED4 On */
  //    BSP_LED_On(LED4);      
  //    osDelay(10);
  //  }
  //  else
  //  { 
  //    osDelay(10);
  //  }
  //}
  //else
  //{
  //  if(yval < ThresholdLow)
  //  {
  //    /* LED6 On */
  //    BSP_LED_On(LED6);
  //    osDelay(10);
  //  }
  //  else if(yval > ThresholdHigh)
  //  {
  //    /* LED3 On */
  //    BSP_LED_On(LED3);
  //    osDelay(10);
  //  } 
  //  else
  //  { 
  //    osDelay(10);
  //  }
  //} 
  
  //BSP_LED_Off(LED3);
  //BSP_LED_Off(LED4);
  //BSP_LED_Off(LED5);
  //BSP_LED_Off(LED6);
}

#define _USE_MATH_DEFINES
#include <math.h>

typedef struct Quaternion {
    double w, x, y, z;
} Quaternion;

typedef struct EulerAngles {
    double roll, pitch, yaw;
} EulerAngles;

Quaternion ToQuaternion(EulerAngles e);
EulerAngles ToEulerAngles(Quaternion q);

Quaternion ToQuaternion(EulerAngles e) {
    
    Quaternion q; 
    // Convert degrees to radians
    e.roll = e.roll * M_PI / 180.0;
    e.pitch = e.pitch * M_PI / 180.0;
    e.yaw = e.yaw * M_PI / 180.0;

    // Calculate trigonometric values
    float cy = cos(e.yaw * 0.5);
    float sy = sin(e.yaw * 0.5);
    float cp = cos(e.pitch * 0.5);
    float sp = sin(e.pitch * 0.5);
    float cr = cos(e.roll * 0.5);
    float sr = sin(e.roll * 0.5);

    // Calculate quaternion values
    q.w = cr * cp * cy + sr * sp * sy;
    q.x = sr * cp * cy - cr * sp * sy;
    q.y = cr * sp * cy + sr * cp * sy;
    q.z = cr * cp * sy - sr * sp * cy;
    return q;
}



// this implementation assumes normalized quaternion
// converts to Euler angles in 3-2-1 sequence
EulerAngles ToEulerAngles(Quaternion q) {
    EulerAngles angles;

    // roll (x-axis rotation)
    double sinr_cosp = 2 * (q.w * q.x + q.y * q.z);
    double cosr_cosp = 1 - 2 * (q.x * q.x + q.y * q.y);
    angles.roll = atan2(sinr_cosp, cosr_cosp);

    // pitch (y-axis rotation)
    double sinp = sqrt(1 + 2 * (q.w * q.y - q.x * q.z));
    double cosp = sqrt(1 - 2 * (q.w * q.y - q.x * q.z));
    angles.pitch = 2 * atan2(sinp, cosp) - M_PI / 2;

    // yaw (z-axis rotation)
    double siny_cosp = 2 * (q.w * q.z + q.x * q.y);
    double cosy_cosp = 1 - 2 * (q.y * q.y + q.z * q.z);
    angles.yaw = atan2(siny_cosp, cosy_cosp);

    return angles;
}