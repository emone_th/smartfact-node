/**
  ******************************************************************************
  * @file           : beep.c
  * @brief          : Source for beep.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 EmOne.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by EmOne under Ultimate Liberty license
  * GPL, the "License"; You may not use this file except in compliance with
  * the License.
  *
  ******************************************************************************
  */

#include "beep.h"
#ifdef USE_BEEP

TIM_HandleTypeDef *htim;

void BEEPER_Init(void *handler)
{

  if(handler != NULL)
    htim = handler;
// Configure timer BEEPER_TIM
     __HAL_RCC_TIM3_CLK_ENABLE();
    htim->Instance->CR1   |= TIM_CR1_ARPE; // Auto-preload enable
    htim->Instance->CCMR1 |= TIM_CCMR1_OC2PE; // Output compare 2 preload enable
    htim->Instance->CCMR1 |= TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2M_1; // PWM mode 1
//	BEEPER_TIM->PSC    = 8; // prescaler
    htim->Instance->PSC    = (SystemCoreClock) / 4000000;
    htim->Instance->ARR    = 999; // auto reload value
    htim->Instance->CCR4   = 499; // 50% duty cycle
    htim->Instance->CCER  |= TIM_CCER_CC4P; // Output polarity
    htim->Instance->CCER  |= TIM_CCER_CC4E; // BEEPER_TIM_CH2 output compare enable
    htim->Instance->EGR    = TIM_EGR_UG; // Generate an update event to reload the prescaler value immediately
    htim->Instance->DIER  |= TIM_DIER_UIE; // ?IMx update interrupt enable

    // BEEPER_TIM IRQ
    HAL_NVIC_SetPriority(BEEPER_TIM_IRQN, 5, 0);
    HAL_NVIC_EnableIRQ(BEEPER_TIM_IRQN);
}

void BEEPER_Enable(uint16_t freq, uint32_t duration)
{
  if (freq < 100 || freq > 8000 || duration == 0) {
    BEEPER_Disable();
  } else {
    _beep_duration = (freq / 100) * duration + 1;

    // Configure and enable PWM timer
    __HAL_RCC_TIM3_CLK_ENABLE();
    htim->Instance->ARR = (SystemCoreClock) / (freq * htim->Instance->PSC) - 1;
    htim->Instance->CCR4 = htim->Instance->ARR >> 1; // 50% duty cycle
    htim->Instance->CR1 |= TIM_CR1_CEN; // Counter enable
  }
}

void BEEPER_Disable(void)
{
  // Counter disable
  htim->Instance->CR1 &= ~TIM_CR1_CEN;
  // Disable TIMx peripheral to conserve power
  __HAL_RCC_TIM3_CLK_DISABLE();

}

void BEEPER_PlayTones(const Tone_TypeDef * tones)
{
  _tones = tones;
  _tones_playing = true;
  BEEPER_Enable(_tones->frequency,_tones->duration);
}

void BEEPER_IRQ( void )
{
  _beep_duration--;
    if (_beep_duration == 0) {
      if (_tones_playing) {
        // Currently playing tones, take next tone
        _tones++;
        if (_tones->frequency == 0 && _tones->duration == 0) {
                // Last tone in sequence
                BEEPER_Disable();
                _tones_playing = false;
                _tones = NULL;
        } else {
                if (_tones->frequency == 0) {
                        // Silence period
                        BEEPER_TIM->ARR = (SystemCoreClock) / (100 * BEEPER_TIM->PSC) - 1;
                        BEEPER_TIM->CCR4 = 0; // 0% duty cycle
                        _beep_duration = _tones->duration + 1;
                } else {
                        // Play next tone in sequence
                        BEEPER_Enable(_tones->frequency,_tones->duration);
                }
        }
      } else {
        BEEPER_Disable();
      }
    }
}
#endif /* USE_BEEP */
