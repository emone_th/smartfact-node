/*
    Example for Ultrasonic2 Click

        Date          : jul 2018.
        Author        : Katarina Perendic

    Test configuration STM32 :

        MCU              : STM32F107VCT6
        Dev. Board       : EasyMx PRO v7 for STM32
        ARM Compiler ver : v6.0.0.0

    ---

    Description :

    The application is composed of three sections :

    - System Initialization -  Initializes UART module and sets CS and AN pins as OUTPUT
    - Application Initialization - Driver initialization for powering the Click board,
                                   chip configuration and reading the sensor diagnosis.
    - Application Task - (code snippet) -  Starts measuring and measures the distance of an object every 500 ms.

    Additional Functions :
         An additional function is used to store responses.

    Notes :
       Bear in mind that any change in device initialization drastically changes the work load of the device and its measurement pattern.
       This library has been made according to Texas Instruments PGA460 chip library.

    Maintain by : Anol P. <anol.p@emone.co.th> 2020
*/

#include "Click_Ultrasonic2_config.h"
#include "main.h"
#ifdef  USE_PGA460
#include "slac741i/PGA460_USSC.h"
#elif defined (USE_MB7389)
#include "adc.h"
extern ADC_HandleTypeDef hadc1;
#else
//#error "Please define ultrasonic type in main.h"
#endif
#ifdef USE_ULTRASONIC_SENSOR
ULTRASONIC_OBJ_t us_sensor;
ULTRASONIC_t ultrasonic_obj[_ULTRASONIC2_DETECT_OBJECT_2];
char demoText[50] = {0};
int idx = 0;
#ifdef  USE_PGA460
const uint8_t _ULTRASONIC2_TVG_CFG[ 7 ] =
{
    0x99, 0x99, 0x88, 0x82, 0x08, 0x55, 0xC0
};

const uint8_t _ULTRASONIC2_THRESHOLDS_P1_CFG[ 16 ] =
{
    0x88, 0x88, 0x88, 0x88, 0x88, 0x88,
    0x84, 0x21, 0x08, 0x42, 0x10, 0x80,
    0x80, 0x80, 0x80, 0x00
};

const uint8_t _ULTRASONIC2_THRESHOLDS_P2_CFG[ 16 ] =
{
    0x88, 0x88, 0x88, 0x88, 0x88, 0x88,
    0x84, 0x21, 0x08, 0x42, 0x10, 0x80,
    0x80, 0x80, 0x80, 0x00
};

const uint32_t _ULTRASONIC2_UART_CFG [ 4 ] =
{
    115200,
    8, //_UART_8_BIT_DATA,
    0, //_UART_NOPARITY,
    1  //_UART_ONE_STOPBIT
};
#endif

UART_HandleTypeDef *huart;

void ultrasonic_systemInit(void)
{
//	HAL_Delay(2000);
	//TODO: Set pin mux
	//    mikrobus_gpioInit( _MIKROBUS1, _MIKROBUS_AN_PIN, _GPIO_OUTPUT );
	//    mikrobus_gpioInit( _MIKROBUS1, _MIKROBUS_CS_PIN, _GPIO_OUTPUT );
	//    mikrobus_uartInit( _MIKROBUS1, &_ULTRASONIC2_UART_CFG[0] );
        if (HAL_UART_DeInit(huart) != HAL_OK)
        {
          Error_Handler();
        }
#ifdef  USE_PGA460
        huart->Init.BaudRate = 115200;
        huart->Init.WordLength = UART_WORDLENGTH_8B;
        huart->Init.StopBits = UART_STOPBITS_2;
        huart->Init.Mode = UART_MODE_TX_RX;
#elif defined (USE_MB7389)
        huart->Init.BaudRate = 9600;
        huart->Init.WordLength = UART_WORDLENGTH_8B;
        huart->Init.StopBits = UART_STOPBITS_1;
        huart->Init.Mode = UART_MODE_RX;
#else
#endif/* USE_PGA460 */
        huart->Init.Parity = UART_PARITY_NONE;
        huart->Init.HwFlowCtl = UART_HWCONTROL_NONE;
        huart->Init.OverSampling = UART_OVERSAMPLING_16;

        if (HAL_UART_Init(huart) != HAL_OK)
        {
          Error_Handler();
        }

	/* UART interrupt */
	//    RXNEIE_USART3_CR1_bit = 1;
	//    NVIC_IntEnable( IVT_INT_USART3 );
	//    __HAL_UART_ENABLE_IT(&huart3);
	HAL_NVIC_SetPriority(USART3_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(USART3_IRQn);
        
        HAL_UART_Receive_IT(huart, &demoText[idx], 1);
	HAL_Delay(100);
}

ULTRASONIC_DIAG_t *dg;
void ultrasonic_applicationInit(void *obj)
{
  if (obj != NULL) {
        huart = obj;
        us_sensor.DETECT_OBJECT = _ULTRASONIC2_DETECT_OBJECT_2;
        us_sensor.obj = (ULTRASONIC_t *) &ultrasonic_obj;
        dg = (ULTRASONIC_DIAG_t*) &us_sensor.diag;

	//    ultrasonic2_uartDriverInit( (T_ULTRASONIC2_P)&_MIKROBUS1_GPIO, (T_ULTRASONIC2_P)&_MIKROBUS1_UART );
	ultrasonic2_uartDriverInit(NULL, NULL);

	/* INIT */
	
	//    ultrasonic2_setAmbientTemperature( 25 );
	//

        ultrasonic2_enable(_ULTRASONIC2_DEVICE_DISABLE);
#ifdef USE_PGA460
	ultrasonic2_defaultConfiguration();
	//    ultrasonic2_setComunication(_ULTRASONIC2_UART_ADDR_UPDATE_0 );
	pga460_initThresholds(_ULTRASONIC2_TVG_LEVEL_50_PERCENT);
	pga460_initTVG(_ULTRASONIC2_TVG_GAIN_RANGE_52_TO_84db, _ULTRASONIC2_TVG_LEVEL_50_PERCENT);
	//    ultrasonic2_setThresholds( &_ULTRASONIC2_THRESHOLDS_P1_CFG, &_ULTRASONIC2_THRESHOLDS_P2_CFG);
	//    ultrasonic2_setTVG(_ULTRASONIC2_TVG_GAIN_RANGE_32_TO_64db, &_ULTRASONIC2_TVG_CFG[0]);

	//mikrobus_logWrite(" --- Start diagnostics ---", _LOG_LINE);
	//mikrobus_logWrite("-----------------------------------------", _LOG_LINE);

	//  Function for configuration chip. More...

	//Burn EEPROM
	pga460_burnEEPROM();

	/* DIAGNOSTICS */
	ultrasonic2_getDiagnostics(&dg->diagnosticsFreq, &dg->diagnosticsPeriod,
                                &dg->diagnosticsTemperature, &dg->diagnosticsNoise);
	//mikrobus_logWrite("System Diagnostics - Frequency (kHz): ",_LOG_TEXT);
	//    FloatToStr(diagnosticsFreq, demoText);
	//mikrobus_logWrite(demoText, _LOG_LINE);
	//mikrobus_logWrite("System Diagnostics - Decay Period (us): ",_LOG_TEXT);
	//    FloatToStr(diagnosticsPeriod, demoText);
	//mikrobus_logWrite(demoText, _LOG_LINE);
	//mikrobus_logWrite("System Diagnostics - Die Temperature (C): ",_LOG_TEXT);
	//    FloatToStr(diagnosticsTemperature, demoText);
	//mikrobus_logWrite(demoText, _LOG_LINE);
	//mikrobus_logWrite("System Diagnostics - Noise Level: ",_LOG_TEXT);
	//    FloatToStr(diagnosticsNoice, demoText);
	//mikrobus_logWrite(demoText, _LOG_LINE);

	printf("System Diagnostics - Frequency (kHz): %f\r\n", dg->diagnosticsFreq);
	printf("System Diagnostics - Decay Period (us): %f\r\n", dg->diagnosticsPeriod);
	printf("System Diagnostics - Die Temperature (C): %f\r\n", dg->diagnosticsTemperature);
	printf("System Diagnostics - Noise Level:  %f\r\n", dg->diagnosticsNoise);

	ultrasonic2_configuration(_ULTRASONIC2_ECHO_DATA_DUMP_OF_PRESET_1);
        ultrasonic2_configuration(_ULTRASONIC2_ECHO_DATA_DUMP_OF_PRESET_2);
	//mikrobus_logWrite("-----------------------------------------", _LOG_LINE);
	//mikrobus_logWrite("---- Start measurement ----",_LOG_LINE);
	//mikrobus_logWrite("-----------------------------------------", _LOG_LINE);
#else
#endif/* USE_PGA460 */
  }
}

void ultrasonic_applicationTask(bool range)
{
	uint8_t cnt = 0;
	//    char demoText[50] = {0};
	/* RETURNS DATA */
#ifdef USE_PGA460
	if (range) {
		printf("Long Range measurement\r\n");

	} else {
		printf("Short Range measurement\r\n");
	}

        ultrasonic2_configuration(_ULTRASONIC2_ECHO_DATA_DUMP_OF_PRESET_1);
        ultrasonic2_configuration(_ULTRASONIC2_ECHO_DATA_DUMP_OF_PRESET_2);

        us_sensor.diag.diagnosticsTemperature = pga460_runDiagnostics(0, 2);
        ultrasonic2_startUltrasonicMeasurement(range, us_sensor.DETECT_OBJECT);
	pga460_pullUltrasonicMeasResult(true);
#elif USE_MB7389
        ultrasonic2_enable(_ULTRASONIC2_DEVICE_ENABLE);
        HAL_Delay( 500 );
#endif/* USE_PGA460 */

	for (cnt = 0; cnt < us_sensor.DETECT_OBJECT; cnt++) {
		ultrasonic2_getUltrasonicMeasurement(cnt,
                           &us_sensor.obj[cnt].distance,
                           &us_sensor.obj[cnt].width,
                           &us_sensor.obj[cnt].peak);
		//        HAL_Delay(10);
//		if (distance > _ULTRASONIC2_MIN_RANGE) {
//			if (distance > _ULTRASONIC2_MAX_RANGE) {
//				//                //mikrobus_logWrite(" Distance is out of range !!! ",_LOG_LINE);
//				printf(" Distance is out of range !!! \r\n");
//			} else {
				//                FloatToStr(distance, demoText);
				//                demoText[ 7 ] = 0;
				//mikrobus_logWrite(" Distance : ",_LOG_TEXT);
				//mikrobus_logWrite(demoText,_LOG_TEXT);
				//mikrobus_logWrite(" mm", _LOG_LINE);

                  printf("Obj: %d, Distance: %d, Width: %4d, Peak: %3d\r\n",
                  //printf("Obj: %d, Distance: %04.3f, Width: %4d, Peak: %3d\r\n",
                  //printf("Obj: %d, Distance: %d.%3d, Width: %4d, Peak: %3d\r\n",
                            cnt+1,
                                    (int) us_sensor.obj[cnt].distance,
                                    us_sensor.obj[cnt].width,
                                    us_sensor.obj[cnt].peak);
//                  us_sensor.obj[cnt].distance =
//                  us_sensor.obj[cnt].width =
//                  us_sensor.obj[cnt].peak = 0;
//			}
//		}

	}
#ifdef  USE_MB7389
        ultrasonic2_enable(_ULTRASONIC2_DEVICE_DISABLE);
#endif /* USE_MB7389 */
	//    HAL_Delay( 500 );
}

/* UART Interrupt */
void Ultrasonic_RX_ISR(void) //iv IVT_INT_USART3 ics ICS_AUTO
{
	//char tmp;

	//    if( RXNE_USART3_SR_bit )
	//    {
	//        tmp = USART3_DR;
	//tmp = huart->Instance->DR;
	//ultrasonic2_getc(tmp);
	//    }
    
    if(demoText[idx] == '\r' || idx < 50)
    {
      idx = 0;
    }
    else 
    {
      idx++;
    }
    HAL_UART_Receive_IT(huart, &demoText[idx], 1);
}

void ultrasonic2_uartDriverInit(T_ULTRASONIC2_P gpioObj,
    T_ULTRASONIC2_P uartObj)
{
#ifdef USE_PGA460
	pga460_initBoostXLPGA460(0, _ULTRASONIC2_BAUDRATE_115200,
	    _ULTRASONIC2_UART_ADDR_UPDATE_0);
#endif/* USE_PGA460 */
	ultrasonic_systemInit();
}

void ultrasonic2_getc(uint8_t input)
{

}

void ultrasonic2_enable(uint8_t state)
{
	if (state) {
		HAL_GPIO_WritePin(MB1_CS_GPIO_Port, MB1_CS_Pin, GPIO_PIN_RESET);
		HAL_Delay(1000);
		HAL_GPIO_WritePin(MB1_CS_GPIO_Port, MB1_CS_Pin, GPIO_PIN_SET);
	} else {
		HAL_GPIO_WritePin(MB1_CS_GPIO_Port, MB1_CS_Pin, GPIO_PIN_RESET);
	}
}

void ultrasonic2_setAmbientTemperature(int8_t temp)
{
	//  Function for sets ambient temperature. More...
}

void ultrasonic2_configuration(uint8_t echo)
{
#ifdef USE_PGA460
	// Capture echo data dump
	pga460_runEchoDataDump(echo);
	pga460_pullEchoDataDumpBulk();
#endif/* USE_PGA460 */
	//   HAL_Delay(1000);
	//   if (numOfObj == 0 || numOfObj >8) { numOfObj = 1; } // sets number of objects to detect to 1 if invalid input
}

void ultrasonic2_setComunication(uint8_t uartAddrUpdate)
{
#ifdef USE_PGA460
	//  Function for init comunication. More...
	pga460_initBoostXLPGA460(0, _ULTRASONIC2_BAUDRATE_115200,
	    uartAddrUpdate);
#endif/* USE_PGA460 */
}

void ultrasonic2_defaultConfiguration(void)
{
#ifdef USE_PGA460
	//  Function for init device. More...
	pga460_defaultPGA460(0);
#endif/* USE_PGA460 */
}

void ultrasonic2_setThresholds(uint8_t *TH_P1, uint8_t *TH_P2)
{
#ifdef USE_PGA460
	//  Function for init thresholds. More...
	pga460_initThresholds(_ULTRASONIC2_TVG_LEVEL_50_PERCENT);
#endif/* USE_PGA460 */
}

void ultrasonic2_setTVG(uint8_t agr, uint8_t *TVG)
{
#ifdef USE_PGA460
	//  Function for init TVG. More...
	pga460_initTVG(_ULTRASONIC2_TVG_GAIN_RANGE_52_TO_84db,
                       _ULTRASONIC2_TVG_LEVEL_50_PERCENT);
#endif/* USE_PGA460 */
}

void ultrasonic2_sendCommand(uint8_t cmd, uint8_t numObjUpdate)
{
	//  Function for send command. More...
}

void ultrasonic2_startUltrasonicMeasurement(bool range, uint8_t detectObj)
{
	//  Function for start ultrasonic measurement. More...
        #ifdef USE_PGA460
	pga460_ultrasonicCmd(range, detectObj);
        #elif USE_MB7389
        ultrasonic2_enable(_ULTRASONIC2_DEVICE_ENABLE);
        #endif
}

void ultrasonic2_getUltrasonicMeasurement(uint8_t numObj, float *distOUT,
    uint16_t *widthOUT, uint8_t *ampOUT)
{
  
#ifdef USE_PGA460
	//  Function for get ultrasonic measurement. More...
	*distOUT = pga460_printUltrasonicMeasResult(0 + (numObj * 3),
                    us_sensor.diag.diagnosticsTemperature);
	*widthOUT = pga460_printUltrasonicMeasResult(1 + (numObj * 3),
                    us_sensor.diag.diagnosticsTemperature);
	*ampOUT = pga460_printUltrasonicMeasResult(2 + (numObj * 3),
                    us_sensor.diag.diagnosticsTemperature);
#elif USE_MB7389
  if (numObj == 1)
  {
          uint32_t data;
          ADC_ChannelConfTypeDef ch;
          ch.Channel = ADC_CHANNEL_11;
          ch.Rank = 1;
          ch.SamplingTime = ADC_SAMPLETIME_144CYCLES;
          //ch.Offset = 0;
          HAL_ADC_ConfigChannel(&hadc1, &ch);
          HAL_ADC_Start(&hadc1);
          HAL_ADC_PollForConversion(&hadc1, 500);

          data = HAL_ADC_GetValue(&hadc1);
          HAL_ADC_Stop(&hadc1);

          *distOUT = data * 5;
  }
  else if (numObj == 2)
  {
          char *range = NULL;
          char start = 'R';
          char end = '\r';
          int len = 0;

          range = strchr(demoText, start);
          if (strlen(range) < 6); //RXXXX\r
            return;

          range = strpbrk(range, &start);
          range = strtok(range, &end);

          len = strlen(range);

          *distOUT = atoi(range);
  }
#endif/* USE_PGA460 */
//      HAL_Delay(100);
}

void ultrasonic2_getDiagnostics(float *freqOUT, float *periodOUT,
    float *tempOUT, float *noiseOUT)
{
#ifdef USE_PGA460
	*freqOUT = pga460_runDiagnostics(1, 0);
	*periodOUT = pga460_runDiagnostics(0, 1);
	*tempOUT = pga460_runDiagnostics(0, 2);
	*noiseOUT = pga460_runDiagnostics(0, 3);
#endif/* USE_PGA460 */
}

#ifdef USE_PGA460
int pga460_sndSerialData(uint8_t *data, size_t len)
{
    return HAL_UART_Transmit(huart, data, len, 250);
}

int pga460_rcvSerialData(uint8_t *data, size_t len)
{
    return HAL_UART_Receive(huart, data, len, 500);
}
#endif /* USE_PGA460 */
#endif /* USE_ULTRASONIC_SENSOR */
