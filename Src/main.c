/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "fatfs.h"
#include "usb_device.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "WiMOD_LoRaWAN_API.h"

#if defined (MINIZ)
#include "miniz.h"
#endif

#ifdef USE_SENSOR
#ifdef USE_MEMS
#include "stm32f4_discovery_accelerometer.h"
#endif
#ifdef USE_ADC_SENSOR
#include "adc.h"
#endif /* USE_ADC_SENSOR */

#ifdef USE_ADC_SENSOR_CT
//#include "ct.h"
#elif defined (USE_ADC_SENSOR_I2C)

#elif defined (USE_AQI_SENSOR)
#ifdef USE_PM_SENSOR
#include "pm.h"
extern PM_t pm_sensor;
#endif
#ifdef USE_CO2_SENSOR
#include "co2.h"
extern CO2_t co2_sensor;
#endif
#ifdef USE_SOUND_LVL_SENSOR
#include "soundlvl.h"
extern SOUNDLVL_t sl_sensor;
#endif
#elif defined (USE_ULTRASONIC_SENSOR)
#include "Click_Ultrasonic2_config.h"
extern ULTRASONIC_OBJ_t us_sensor;
#elif  defined (USE_ROTARY_ENCODER)
#elif defined ( USE_IMU_I2C )
#include "imu.h"
#endif /* USE_XXX_SENSOR */
#endif /* USE_SENSOR */

#ifdef USE_BEEP
#include "beep.h"
#endif /* USE_BEEP */

#ifdef USE_GPS
#include "gnss1a1_conf.h"
#include "gnss1a1_gnss.h"
#include "teseo_liv3f_conf.h"
#include "gnss_parser.h"
#include "math.h"
#endif /* USE_GPS */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef struct {
	void (*init) (void *handle);
	uint16_t (*read) (void *handle);
} Sensor_Drv_t;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

CRC_HandleTypeDef hcrc;

I2C_HandleTypeDef hi2c1;
I2C_HandleTypeDef hi2c3;

IWDG_HandleTypeDef hiwdg;

RNG_HandleTypeDef hrng;

RTC_HandleTypeDef hrtc;

SPI_HandleTypeDef hspi1;
SPI_HandleTypeDef hspi2;
DMA_HandleTypeDef hdma_spi2_tx;

TIM_HandleTypeDef htim3;

UART_HandleTypeDef huart2;
UART_HandleTypeDef huart3;
UART_HandleTypeDef huart6;

osThreadId defaultTaskHandle;
osThreadId commuTaskHandle;
osThreadId sensorTaskHandle;
osThreadId outputTaskHandle;
osThreadId trackingTaskHandle;
osMessageQId msgQueueHandle;
osTimerId heartbeatTimerHandle;
osTimerId telemetryTimerHandle;
osMutexId coreMutexHandle;
osSemaphoreId coreBinarySemHandle;
/* USER CODE BEGIN PV */
osMailQId msgMailHandle;

evState_t ev;
LoRa_App loraAppStatus = {
    .devAddr.u32 = 0x00000000, //Device address (LSB)
    .nwkSKey = {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
        0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF}, //Network key (MSB)
    .appSKey = {0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99, 0x88,
        0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11, 0x00}, //App key (MSB)
    .appKey = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
        0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10},
    .appEUI = {0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11, 0x22},
    .devEUI = {0x70, 0xB3, 0xD5, 0x8F, 0xFF, 0xFF, 0xFF, 0xFF},
	.period = 6,
};

user_data_t sData_t;
static __IO UINT32 iCounter = 0;
#ifdef USE_GPS
GNSSParser_Data_t gps_data;
#endif /* USE_GPS */
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_CRC_Init(void);
static void MX_IWDG_Init(void);
static void MX_RNG_Init(void);
static void MX_RTC_Init(void);
static void MX_SPI2_Init(void);
static void MX_TIM3_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_USART3_UART_Init(void);
static void MX_USART6_UART_Init(void);
static void MX_ADC1_Init(void);
static void MX_I2C1_Init(void);
static void MX_I2C3_Init(void);
static void MX_SPI1_Init(void);
void StartDefaultTask(void const * argument);
void StartCommuTask(void const * argument);
void StartSensorTask(void const * argument);
void StartOutputTask(void const * argument);
void StartTrackingTask(void const * argument);
void heartbeatCallback(void const * argument);
void telemetryCallback(void const * argument);

/* USER CODE BEGIN PFP */
#ifdef USE_GPS
static void MX_GPS_Init(void);
#endif /* USE_GPS */

//Mode POST_function(Event ev);
//Mode IDLE_function(Event ev);
//Mode SETTING_function(Event ev);
//Mode RUNNING_function(Event ev);
//Mode ALARM_function(Event ev);
//Mode FAILSAFE_function(Event ev);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
#ifdef USE_GPS
int GNSS_PRINT(char *pBuffer)
{
	return printf("%s", pBuffer);
}
#endif /* USE_GPS */

int loraDataRx(uint8_t fport, uint8_t* data, size_t len)
{
    switch(fport) {
    case 99:
       //Restart system
      NVIC_SystemReset();
      break;

    case 3:
      if(len == 1) {
        //lora tx perior
        sData_t.period = loraAppStatus.period = *data;
        ev.commu = RUNNING;
      }
      break;
    case 0xFF:
      if(len > 0)
      {
        if(*data == 0x1)
        {
          printf("Acknowledges ResetInd command\r\n");
        }
      }
      break;
    default: break;
    };
    return 0;
}
/**
 * @brief		Trace output for standard output
 */
int fputc(int ch, FILE *f) {
  return ITM_SendChar(ch);
}

int _read(int file, char *ptr, int len) {
  int DataIdx;

  for (DataIdx = 0; DataIdx < len; DataIdx++) {
    *(uint8_t *)ptr++ = __io_getchar();
  }

  return len;
}

int _write(int file, char *ptr, int len) {
  int DataIdx;

  osSemaphoreWait(coreBinarySemHandle, 5000);

  for (DataIdx = 0; DataIdx < len; DataIdx++) {
    __io_putchar(*ptr++);
  }

  osSemaphoreRelease(coreBinarySemHandle);

  return len;
}

int __io_putchar(int ch) {
  return HAL_UART_Transmit(&huart2, (uint8_t *)&ch, 1, 500) == HAL_OK ? 0 : -1;
}
/* USER CODE END 0 */

/**y
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
 {
  /* USER CODE BEGIN 1 */
  //HAL_DBGMCU_EnableDBGSleepMode();
  //HAL_DBGMCU_EnableDBGStandbyMode();
  //HAL_DBGMCU_EnableDBGStopMode();
  iCounter = 0;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
  osSemaphoreDef(coreBinarySem);
  coreBinarySemHandle = osSemaphoreCreate(osSemaphore(coreBinarySem), 1);

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
  
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_CRC_Init();
  MX_IWDG_Init();
  MX_RNG_Init();
  MX_RTC_Init();
  MX_SPI2_Init();
  MX_TIM3_Init();
  MX_USART2_UART_Init();
  MX_USART3_UART_Init();
  MX_USART6_UART_Init();
  MX_FATFS_Init();
  MX_ADC1_Init();
  MX_I2C1_Init();
  MX_I2C3_Init();
  MX_SPI1_Init();
  /* USER CODE BEGIN 2 */

  BSP_LED_Init(LED3);
  BSP_LED_Init(LED4);
  BSP_LED_Init(LED5);
  BSP_LED_Init(LED6);
  BSP_PB_Init(BUTTON_KEY, BUTTON_MODE_EXTI);
  /* USER CODE END 2 */

  /* Create the mutex(es) */
  /* definition and creation of coreMutex */
  //osMutexDef(coreMutex);
  //coreMutexHandle = osMutexCreate(osMutex(coreMutex));

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* definition and creation of coreBinarySem */
//  osSemaphoreDef(coreBinarySem);
//  coreBinarySemHandle = osSemaphoreCreate(osSemaphore(coreBinarySem), 1);

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* Create the timer(s) */
  /* definition and creation of heartbeatTimer */
  //osTimerDef(heartbeatTimer, heartbeatCallback);
  //heartbeatTimerHandle = osTimerCreate(osTimer(heartbeatTimer), osTimerPeriodic, NULL);

  /* definition and creation of telemetryTimer */
  //osTimerDef(telemetryTimer, telemetryCallback);
  //telemetryTimerHandle = osTimerCreate(osTimer(telemetryTimer), osTimerOnce, NULL);

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* definition and creation of msgQueue */
  //osMessageQDef(msgQueue, 16, uint32_t);
  //msgQueueHandle = osMessageCreate(osMessageQ(msgQueue), NULL);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  //osMailQDef(msgMail, 1, typeof(sData_t));
  //msgMailHandle = osMailCreate(osMailQ(msgMail), NULL);
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of commuTask */
  //osThreadDef(commuTask, StartCommuTask, osPriorityNormal, 0, 1024);
  //commuTaskHandle = osThreadCreate(osThread(commuTask), NULL);

  /* definition and creation of sensorTask */
  osThreadDef(sensorTask, StartSensorTask, osPriorityNormal, 0, 1024);
  sensorTaskHandle = osThreadCreate(osThread(sensorTask), NULL);

  /* definition and creation of outputTask */
  //osThreadDef(outputTask, StartOutputTask, osPriorityNormal, 0, 128);
  //outputTaskHandle = osThreadCreate(osThread(outputTask), NULL);

#ifdef USE_GPS
  /* definition and creation of trackingTask */
  osThreadDef(trackingTask, StartTrackingTask, osPriorityNormal, 0, 1024);
  trackingTaskHandle = osThreadCreate(osThread(trackingTask), NULL);
#endif

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1) {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
  PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc1.Init.Resolution = ADC_RESOLUTION_10B;
  hadc1.Init.ScanConvMode = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  //sConfig.Channel = ADC_CHANNEL_TEMPSENSOR;
  //sConfig.Rank = 1;
  //sConfig.SamplingTime = ADC_SAMPLETIME_144CYCLES;
  //if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  //{
  //  Error_Handler();
  //}
  /* USER CODE BEGIN ADC1_Init 2 */
  sConfig.Channel = ADC_CHANNEL_11;
  sConfig.Rank = 1;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  //sConfig.Channel = ADC_CHANNEL_15;
  //sConfig.Rank = 3;
  //if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  //{
  //  Error_Handler();
  //}
  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief CRC Initialization Function
  * @param None
  * @retval None
  */
static void MX_CRC_Init(void)
{

  /* USER CODE BEGIN CRC_Init 0 */

  /* USER CODE END CRC_Init 0 */

  /* USER CODE BEGIN CRC_Init 1 */

  /* USER CODE END CRC_Init 1 */
  hcrc.Instance = CRC;
  if (HAL_CRC_Init(&hcrc) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CRC_Init 2 */

  /* USER CODE END CRC_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief I2C3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C3_Init(void)
{

  /* USER CODE BEGIN I2C3_Init 0 */

  /* USER CODE END I2C3_Init 0 */

  /* USER CODE BEGIN I2C3_Init 1 */

  /* USER CODE END I2C3_Init 1 */
  hi2c3.Instance = I2C3;
  hi2c3.Init.ClockSpeed = 100000;
  hi2c3.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c3.Init.OwnAddress1 = 0;
  hi2c3.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c3.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c3.Init.OwnAddress2 = 0;
  hi2c3.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c3.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C3_Init 2 */

  /* USER CODE END I2C3_Init 2 */

}

/**
  * @brief IWDG Initialization Function
  * @param None
  * @retval None
  */
static void MX_IWDG_Init(void)
{

  /* USER CODE BEGIN IWDG_Init 0 */

  /* USER CODE END IWDG_Init 0 */

  /* USER CODE BEGIN IWDG_Init 1 */

  /* USER CODE END IWDG_Init 1 */
  hiwdg.Instance = IWDG;
  hiwdg.Init.Prescaler = IWDG_PRESCALER_256;
  hiwdg.Init.Reload = 4095;
  if (HAL_IWDG_Init(&hiwdg) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN IWDG_Init 2 */

  /* USER CODE END IWDG_Init 2 */

}

/**
  * @brief RNG Initialization Function
  * @param None
  * @retval None
  */
static void MX_RNG_Init(void)
{

  /* USER CODE BEGIN RNG_Init 0 */

  /* USER CODE END RNG_Init 0 */

  /* USER CODE BEGIN RNG_Init 1 */

  /* USER CODE END RNG_Init 1 */
  hrng.Instance = RNG;
  if (HAL_RNG_Init(&hrng) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN RNG_Init 2 */

  /* USER CODE END RNG_Init 2 */

}

/**
  * @brief RTC Initialization Function
  * @param None
  * @retval None
  */
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */
  uint32_t bkup;
  /* USER CODE END RTC_Init 0 */

  RTC_TimeTypeDef sTime = {0};
  RTC_DateTypeDef sDate = {0};
  RTC_AlarmTypeDef sAlarm = {0};

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */
  /** Initialize RTC Only
  */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }

  /* USER CODE BEGIN Check_RTC_BKUP */
  bkup = HAL_RTCEx_BKUPRead(&hrtc, 0);
  if(bkup != 0)
  {
    sDate.Year = (uint8_t)((bkup % 100));
    sDate.Month = (uint8_t)((bkup / 100) % 100);
    sDate.Date = (uint8_t)(bkup / 10000);
    if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK) {
        Error_Handler();
    }

    bkup = HAL_RTCEx_BKUPRead(&hrtc, 1);
    sTime.Hours = (uint8_t) (bkup / 10000);
    sTime.Minutes = (uint8_t)((bkup / 100) % 100);
    sTime.Seconds = (uint8_t)((bkup % 100));
    if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK) {
      Error_Handler();
    }
    return;
  }

  /* USER CODE END Check_RTC_BKUP */

  /** Initialize RTC and set the Time and Date
  */
  sTime.Hours = 0x0;
  sTime.Minutes = 0x0;
  sTime.Seconds = 0x0;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  sDate.WeekDay = RTC_WEEKDAY_MONDAY;
  sDate.Month = RTC_MONTH_JANUARY;
  sDate.Date = 0x1;
  sDate.Year = 0x0;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  /** Enable the Alarm A
  */
  sAlarm.AlarmTime.Hours = 0x0;
  sAlarm.AlarmTime.Minutes = 0x0;
  sAlarm.AlarmTime.Seconds = 0x0;
  sAlarm.AlarmTime.SubSeconds = 0x0;
  sAlarm.AlarmTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sAlarm.AlarmTime.StoreOperation = RTC_STOREOPERATION_RESET;
  sAlarm.AlarmMask = RTC_ALARMMASK_NONE;
  sAlarm.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL;
  sAlarm.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_DATE;
  sAlarm.AlarmDateWeekDay = 0x1;
  sAlarm.Alarm = RTC_ALARM_A;
  if (HAL_RTC_SetAlarm_IT(&hrtc, &sAlarm, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  /** Enable the WakeUp
  */
  if (HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, 0, RTC_WAKEUPCLOCK_RTCCLK_DIV16) != HAL_OK)
  {
    Error_Handler();
  }
  /** Enable Calibrartion
  */
  if (HAL_RTCEx_SetCalibrationOutPut(&hrtc, RTC_CALIBOUTPUT_1HZ) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN RTC_Init 2 */

  /* USER CODE END RTC_Init 2 */

}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI2_Init(void)
{

  /* USER CODE BEGIN SPI2_Init 0 */

  /* USER CODE END SPI2_Init 0 */

  /* USER CODE BEGIN SPI2_Init 1 */

  /* USER CODE END SPI2_Init 1 */
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 0;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 1000;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 9600;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  /* USER CODE END USART3_Init 2 */

}

/**
  * @brief USART6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART6_UART_Init(void)
{

  /* USER CODE BEGIN USART6_Init 0 */

  /* USER CODE END USART6_Init 0 */

  /* USER CODE BEGIN USART6_Init 1 */

  /* USER CODE END USART6_Init 1 */
  huart6.Instance = USART6;
  huart6.Init.BaudRate = 115200;
  huart6.Init.WordLength = UART_WORDLENGTH_8B;
  huart6.Init.StopBits = UART_STOPBITS_1;
  huart6.Init.Parity = UART_PARITY_NONE;
  huart6.Init.Mode = UART_MODE_TX_RX;
  huart6.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart6.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart6) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART6_Init 2 */

  /* USER CODE END USART6_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Stream4_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream4_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream4_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, MB3_RST_Pin|MB3_CS_Pin|MB1_RST_Pin|MB2_CS_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(MB1_PWM_GPIO_Port, MB1_PWM_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(MB3_PWM_GPIO_Port, MB3_PWM_Pin, GPIO_PIN_RESET);

/*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(MB3_INT_GPIO_Port, MB3_INT_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(MB1_CS_GPIO_Port, MB1_CS_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, LD4_Pin|LD3_Pin|LD5_Pin|LD6_Pin
                          |MB4_CS_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(MB4_RST_GPIO_Port, MB4_RST_Pin, GPIO_PIN_SET);

  /*Configure GPIO pins : PE2 PE3 PE6 PE8
                           PE9 PE11 PE12 PE13
                           PE15 PE0 PE1 */
  GPIO_InitStruct.Pin = GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_6|GPIO_PIN_8
                          |GPIO_PIN_9|GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13
                          |GPIO_PIN_15|GPIO_PIN_0|GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : MB3_RST_Pin MB3_CS_Pin MB1_RST_Pin */
  GPIO_InitStruct.Pin = MB3_RST_Pin|MB3_CS_Pin|MB1_RST_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : PC0 PC3 PC4 PC8
                           PC10 PC11 PC12 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_8
                          |GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : MB2_AN_Pin */
  GPIO_InitStruct.Pin = MB2_AN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(MB2_AN_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : USER_BTN_Pin */
  GPIO_InitStruct.Pin = USER_BTN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(USER_BTN_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : MB1_PWM_Pin */
  GPIO_InitStruct.Pin = MB1_PWM_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(MB1_PWM_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PA4 PA5 PA6 PA7
                           PA9 PA10 PA15 */
  GPIO_InitStruct.Pin = GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7
                          |GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PB0 PB2 PB6 PB9 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_2|GPIO_PIN_6|GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : MB3_PWM_Pin */
  GPIO_InitStruct.Pin = MB3_PWM_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(MB3_PWM_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : MB1_INT_Pin */
  GPIO_InitStruct.Pin = MB1_INT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(MB1_INT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : MB2_CS_Pin */
  GPIO_InitStruct.Pin = MB2_CS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(MB2_CS_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : MB1_CS_Pin */
  GPIO_InitStruct.Pin = MB1_CS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(MB1_CS_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PD8 PD9 PD10 PD11
                           PD0 PD1 PD2 PD3
                           PD4 PD5 */
  GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11
                          |GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3
                          |GPIO_PIN_4|GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : LD4_Pin LD3_Pin LD5_Pin LD6_Pin
                           MB4_CS_Pin */
  GPIO_InitStruct.Pin = LD4_Pin|LD3_Pin|LD5_Pin|LD6_Pin
                          |MB4_CS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : MB4_RST_Pin */
  GPIO_InitStruct.Pin = MB4_RST_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(MB4_RST_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : MB3_INT_Pin */
  GPIO_InitStruct.Pin = MB3_INT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(MB3_INT_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

  HAL_NVIC_SetPriority(EXTI2_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(EXTI2_IRQn);

  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

}

/* USER CODE BEGIN 4 */
#ifdef USE_GPS
static void MX_GPS_Init(void)
{
  if (HAL_UART_DeInit(&huart3) != HAL_OK) {
    Error_Handler();
  }

  huart3.Instance = USART3;
  huart3.Init.BaudRate = 9600;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart3) != HAL_OK) {
    Error_Handler();
  }
  GNSS1A1_GNSS_Init(GNSS1A1_TESEO_LIV3F);
}
#endif
/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* init code for USB_DEVICE */
  MX_USB_DEVICE_Init();
  /* USER CODE BEGIN 5 */
  //osTimerStart(heartbeatTimerHandle, 1000);

  /* Infinite loop */
  for (;;) {
    //Event cur_event = event_check();
    //		switch(currentMode) {
    //			case POST:
    //				currentMode = POST_function(cur_event);
    //				break;
    //			case IDLE:
    //				currentMode = IDLE_function(cur_event);
    //				break;
    //			case SETTING:
    //				currentMode = SETTING_function(cur_event);
    //				break;
    //			case RUNNING:
    //				currentMode = RUNNING_function(cur_event);
    //				break;
    //			case ALARM:
    //				currentMode = ALARM_function(cur_event);
    //				break;
    //			case FAILSAFE:
    //				currentMode = FAILSAFE_function(cur_event);
    //				break;
    //			default:
    //				currentMode = currentMode;
    //		}
    if(iCounter > 36000){ //Restart perior 3 hour / 300ms = 36000
      osDelay(5000);
      NVIC_SystemReset();
    }
    iCounter++;
    heartbeatCallback(NULL);
    HAL_IWDG_Refresh(&hiwdg);
    osDelay(300);
  }
  /* USER CODE END 5 */
}

/* USER CODE BEGIN Header_StartCommuTask */
/**
* @brief Function implementing the commuTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartCommuTask */
void StartCommuTask(void const * argument)
{
  /* USER CODE BEGIN StartCommuTask */
  osEvent _event;
  ev.commu = POST;
//  uint8_t mac_cmd[] = {0x01, 0x02};
//  uint8_t buf[16] = {};
//  struct user_data_t *st;

  WiMOD_LoRaWAN_Init(&huart6);
  /* Infinite loop */
  for (;;) {
    switch (ev.commu) {
    case POST:
      while (Ping() == 0)
        osDelay(1000);
      //      Reset();
      //HAL_GPIO_WritePin(MB4_RST_GPIO_Port, MB4_RST_Pin, GPIO_PIN_SET);
      //osDelay(100);
      //HAL_GPIO_WritePin(MB4_RST_GPIO_Port, MB4_RST_Pin, GPIO_PIN_RESET);
      //osDelay(100);
      //HAL_GPIO_WritePin(MB4_RST_GPIO_Port, MB4_RST_Pin, GPIO_PIN_SET);
      //osDelay(1000);
      //      FactoryReset();
      //      osDelay(10000);
      //        LORA_state = LORA_RUNNING;

      ev.commu = SETTING;
      break;
    case IDLE:
      //      osThreadSuspend(&commuTaskHandle);
//      WiMOD_LoRaWAN_SetMAC_CMD(&mac_cmd, sizeof(mac_cmd));
      osDelay(5000 * loraAppStatus.period);
      if (GetNwkStatus() != 1) {
        //        LORA_state = LORA_IDLE;
        ev.commu = FAILSAFE;
        //      else
        //        LORA_state = LORA_ERROR;
      }
      else
      {
        osDelay(5000);

        if (loraAppStatus.maxPlayloadSize < loraAppStatus.playloadSize)
           ev.commu = FAILSAFE;
        else
           ev.commu = RUNNING;
      }

      osDelay(1);
      break;
    case SETTING:
      GetOPMODE();
      osDelay(1000);
      
      SetOPMODE(3);
      osDelay(5000);
      
      SetLinkADR();
      osDelay(1000);

      if (loraAppStatus.opMode != 0) {
        
        SetDevEUI();
        osDelay(5000);
        SetOPMODE(0);
        osDelay(1000);
      }
      SetRadioStack();
      osDelay(1000);
      GetDeviceInfo();
      osDelay(5000);

      GetDevEUI();
      osDelay(5000);

      Deactivate();
      osDelay(500);

      ActivateABP();
      osDelay(5000);

//      WiMOD_LoRaWAN_SetMAC_CMD(&mac_cmd, sizeof(mac_cmd));
      //osTimerStart(telemetryTimerHandle, 5000 * loraAppStatus.period);
//      osDelay(5000);
      ev.commu = IDLE;
      break;
    case RUNNING:


//      if(GetDeviceStatus() != 0) {
//        ev.commu = FAILSAFE;
//        break;
//      }
      telemetryCallback(NULL);
      //ev.commu = ALARM;
      //BSP_LED_Toggle(LED6);
      osDelay(1000);
      break;
    case ALARM:
      
      //osDelay(1000);
      //osTimerStart(telemetryTimerHandle, 5000 * loraAppStatus.period);
      ev.commu = IDLE;

      break;
    case FAILSAFE:
      osMailFree(msgMailHandle, &sData_t);
      //Reactivate();

      osDelay(5000);
      NVIC_SystemReset();
      ev.commu = POST;

      break;
    default:
      ev.commu = POST;
      break;
    }
    osDelay(1);
  }
  /* USER CODE END StartCommuTask */
}

/* USER CODE BEGIN Header_StartSensorTask */
/**
* @brief Function implementing the sensorTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartSensorTask */
void StartSensorTask(void const * argument)
{
  /* USER CODE BEGIN StartSensorTask */

#ifdef USE_SENSOR
  //Sensor initialize
#if defined ( USE_ULTRASONIC_SENSOR )
  
#elif defined (USE_ADC_SENSOR)
  sData_t.period = loraAppStatus.period;
#elif defined (USE_AQI_SENSOR)
  uint16_t co2, temp;
 #elif defined (USE_ROTARY_ENCODER)
  rotary_encoder_init($hspi1)
#else
#warning	"Initialze input sensor do you need."
#endif
#endif /* USE_SENSOR */

  ev.sensor = POST;
  /* Infinite loop */
  for (;;) {
    //TODO: Sensor read

    switch (ev.sensor) {
    case POST:

#ifdef USE_SENSOR
#ifdef USE_MEMS
  if(BSP_ACCELERO_Init() != HAL_OK)
  {
    /* Initialization Error */
    ev.sensor = FAILSAFE;
  }
#ifdef USE_IMU_I2C
  if(IMU_Initialize() != HAL_OK)
  {
    ev.sensor = FAILSAFE;
  }
#endif
#endif
#ifdef USE_ADC_SENSOR_CT
      ADC_Initialize(&hadc1, &loraAppStatus.period);
#elif defined (USE_ADC_SENSOR_I2C)
      ADC_Initialize(&hi2c3, &loraAppStatus.period);
#elif defined ( USE_ADC_SENSOR_4_20MA )
      ADC_Initialize(&hspi1, &loraAppStatus.period);      
#elif defined (USE_AQI_SENSOR)
      PM_Initialize(&huart3);
      CO2_Initialize(&hi2c1);
      SL_Initialize(&hadc1);
#elif defined (USE_ULTRASONIC_SENSOR)
      ultrasonic_applicationInit(&huart3);
#elif defined ( USE_ENCODER )
#if defined ( USE_ROTARY_ENCODER )
    encoder_system_init();
#endif

#endif /* USE_XXX_SENSOR */
#endif /* USE_SENSOR */

      ev.sensor = SETTING;
      break;
    case IDLE:


      osDelay(10000);
      ev.sensor = RUNNING;
      break;
    case SETTING:

#ifdef USE_SENSOR

#if defined (USE_ADC_SENSOR)
      ADC_StartRead(&loraAppStatus.period);
#elif defined (USE_ADC_SENSOR_I2C)
  
#elif defined (USE_AQI_SENSOR)

#elif defined (USE_ULTRASONIC_SENSOR)
#elif defined ( USE_ENCODER )
#if defined ( USE_ROTARY_ENCODER )
      encoder_application_init();
#endif
#else
	#warning	"Choice input sensor do you need."
#endif /* USE_XXX_SENSOR */
#endif /* USE_SENSOR */

      osDelay(1);
      ev.sensor = RUNNING;
      break;
    case RUNNING:

#ifdef USE_SENSOR
#ifdef USE_MEMS
  ACCELERO_ReadAcc();
#endif 
#if defined (USE_ULTRASONIC_SENSOR)
      ultrasonic_applicationTask(true);
#elif defined (USE_ADC_SENSOR)
//      ADC_StartRead(&loraAppStatus.period);
#elif defined (USE_AQI_SENSOR)
      PM_StartMeasurement();

      HAL_Delay(10000);

      PM_ReadParticle(&pm_sensor.pm2_5_val, &pm_sensor.pm10_val);
      PM_StopMeasurement();

      CO2_StartMeasure(&co2, &temp);

      SL_StartMeasure(&sl_sensor);
#elif defined ( USE_ENCODER )
#if defined ( USE_ROTARY_ENCODER )
      encoder_application_task ();
#endif
#elif defined ( USE_MEMS )

#else
	#warning	"Choice input sensor do you need."
#endif /* USE_XXX_SENSOR */
#endif /* USE_SENSOR */

      BSP_LED_Toggle(LED3);
      ev.sensor = ALARM;
      break;
    case ALARM:

#ifdef USE_SENSOR
#ifdef USE_ADC_SENSOR_CT

#elif defined (USE_ADC_SENSOR_I2C)

#elif defined (USE_AQI_SENSOR)

#elif defined (USE_ULTRASONIC_SENSOR)

#endif /* USE_XXX_SENSOR */
#endif /* USE_SENSOR */

      //Beep or LED indicating
      ev.sensor = IDLE;
      break;
    case FAILSAFE:

#ifdef USE_SENSOR
#ifdef USE_ADC_SENSOR_CT

#elif defined (USE_ADC_SENSOR_I2C)

#elif defined (USE_AQI_SENSOR)

#elif defined (USE_ULTRASONIC_SENSOR)

#endif /* USE_XXX_SENSOR */
#endif /* USE_SENSOR */

      ev.sensor = POST;
      break;
    default:
      ev.sensor = POST;
      break;
    }
    osDelay(1);
  }
  /* USER CODE END StartSensorTask */
}

/* USER CODE BEGIN Header_StartOutputTask */
/**
* @brief Function implementing the outputTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartOutputTask */
void StartOutputTask(void const * argument)
{
  /* USER CODE BEGIN StartOutputTask */

#ifdef USE_OUTPUT
#if defined ( USE_BEEP )
  uint32_t previous_freq = 0;
//  uint16_t dutyCycle = 10;
//  TIM_OC_InitTypeDef sConfigOC = {0};
#elif defined ( USE_INDICATOR )
   BSP_LED_Init(LED_EXT01);

#endif
#endif /* USE_OUTPUT */

  ev.output = POST;

  /* Infinite loop */
  for (;;) {
    switch (ev.output) {
    case POST:

#ifdef USE_OUTPUT
#if defined ( USE_BEEP )
      BEEPER_Init(&htim3);
      BEEPER_PlayTones(tones_startup);
#elif defined ( USE_INDICATOR )
      BSP_LED_On(LED_EXT01);
#endif

#endif /* USE_OUTPUT */
      osDelay(1000);
      ev.output = SETTING;
      break;
    case IDLE:

#ifdef USE_OUTPUT
#if defined ( USE_BEEP )
      ev.output = (previous_freq == key_note_freq) ? SETTING : POST;
#elif defined ( USE_INDICATOR )
      BSP_LED_On(LED_EXT01);
      osDelay(250);
      BSP_LED_Off(LED_EXT01);
      osDelay(250);
      BSP_LED_On(LED_EXT01);
      osDelay(250);

#endif

#endif /* USE_OUTPUT */

      osDelay(1);
      ev.output = RUNNING;
      break;
    case SETTING:
#ifdef USE_OUTPUT

#if defined ( USE_BEEP )
      BEEPER_PlayTones(tones_SMB);
#elif defined ( USE_INDICATOR )

#endif
#endif /* USE_OUTPUT */
      osDelay(1);
      ev.output = RUNNING;
      break;
    case RUNNING:
#ifdef USE_OUTPUT

#if defined ( USE_BEEP )

#elif defined ( USE_INDICATOR )
      BSP_LED_Off(LED_EXT01);
      osDelay(1250);
#endif
#endif /* USE_OUTPUT */
      osDelay(1);
      ev.output = ALARM;
      break;
    case ALARM:

#ifdef USE_OUTPUT

#if defined ( USE_BEEP )
      BEEPER_PlayTones(tones_3beep);
#elif defined ( USE_INDICATOR )

#endif
#endif /* USE_OUTPUT */

      osDelay(1000);
      ev.output = IDLE;
      break;
    case FAILSAFE:

#ifdef USE_OUTPUT

#if defined ( USE_BEEP )

#elif defined ( USE_INDICATOR )

#endif
#endif /* USE_OUTPUT */

      ev.output = POST;
      break;
    default:
      ev.output = POST;
      break;
    }
    osDelay(1);
  }
  /* USER CODE END StartOutputTask */
}

/* USER CODE BEGIN Header_StartTrackingTask */
/**
* @brief Function implementing the trackingTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTrackingTask */
#ifdef USE_GPS
void StartTrackingTask(void const * argument)
{
  /* USER CODE BEGIN StartTrackingTask */
#ifdef USE_GPS
  const GNSS1A1_GNSS_Msg_t *gnssMsg;
  RTC_TimeTypeDef sTime = {0};
  RTC_DateTypeDef sDate = {0};

#endif
  ev.tracking = POST;
  /* Infinite loop */
  for (;;) {
    switch (ev.tracking) {
    case POST:
#ifdef USE_GPS
      GNSS_PARSER_Init(&gps_data);
      MX_GPS_Init();
#endif
      ev.tracking = SETTING;
      break;
    case IDLE:
      osDelay(1);
      ev.tracking = RUNNING;
      break;
    case SETTING:
      osDelay(1);
      ev.tracking = RUNNING;
      break;
    case RUNNING:
#ifdef USE_GPS
      gnssMsg = GNSS1A1_GNSS_GetMessage(GNSS1A1_TESEO_LIV3F);
      if(gnssMsg != NULL)
      {

        printf("%s\r\n",(char*)gnssMsg->buf);
        if(GNSS_PARSER_CheckSanity(gnssMsg->buf, gnssMsg->len) == GNSS_PARSER_OK)
        {
          for(int i =0; i< PSTMSAVEPAR; i ++) {
            if(GNSS_PARSER_ParseMsg(&gps_data, i, gnssMsg->buf) == GNSS_PARSER_OK)
            {
              if(i == GPRMC)
              {
                HAL_RTC_GetDate(&hrtc, &sDate, RTC_FORMAT_BCD);
                HAL_RTC_GetTime(&hrtc, &sTime, RTC_FORMAT_BCD);
                if(sTime.Hours != gps_data.gprmc_data.utc.hh &&
                        sTime.Minutes != gps_data.gprmc_data.utc.mm)
                {
                  sTime.Hours = gps_data.gprmc_data.utc.hh;
                  sTime.Minutes = gps_data.gprmc_data.utc.mm;
                  sTime.Seconds = gps_data.gprmc_data.utc.ss;
                  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK) {
                    Error_Handler();
                  }
                  HAL_RTCEx_BKUPWrite(&hrtc, 1, gps_data.gprmc_data.utc.utc);
                }
                if ((sDate.Year != (uint8_t)((gps_data.gprmc_data.date % 100))) &&
                  (sDate.Month != (uint8_t)((gps_data.gprmc_data.date / 100) % 100)) &&
                  (sDate.Date != (uint8_t)(gps_data.gprmc_data.date / 10000)))
                {
                  sDate.Year = (uint8_t)((gps_data.gprmc_data.date % 100));
                  sDate.Month = (uint8_t)((gps_data.gprmc_data.date / 100) % 100);
                  sDate.Date = (uint8_t)(gps_data.gprmc_data.date / 10000);
                  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK) {
                      Error_Handler();
                  }
                  HAL_RTCEx_BKUPWrite(&hrtc, 0, gps_data.gprmc_data.date);
                }
              }
              break;
            }
          }
        }
        GNSS1A1_GNSS_ReleaseMessage(GNSS1A1_TESEO_LIV3F, gnssMsg);
      }
#endif

      ev.tracking = ALARM;
      break;
    case ALARM:
      //Beep or LED indicating
      BSP_LED_Toggle(LED5);
      ev.tracking = IDLE;
      break;
    case FAILSAFE:
      ev.tracking = POST;
      break;
    default:
      ev.tracking = POST;
      break;
    }
    osDelay(1);
  }
  /* USER CODE END StartTrackingTask */
}
#endif

/* heartbeatCallback function */
void heartbeatCallback(void const * argument)
{
  /* USER CODE BEGIN heartbeatCallback */
  //osTimerStop(heartbeatTimerHandle);
  BSP_LED_Toggle(LED4);
  //iCounter++;
  //osTimerStart(heartbeatTimerHandle, 500);
  /* USER CODE END heartbeatCallback */
}

/* telemetryCallback function */
void telemetryCallback(void const * argument)
{
  /* USER CODE BEGIN telemetryCallback */
      static uint8_t buf[16];
      static uint8_t len;
      uint8_t fport;
      uint32_t tmp;
      float deg, minute;
//      osEvent _event;
//      user_data_t* st;
      //osTimerStop(telemetryTimerHandle);
      //if (GetNwkStatus() != 1) {
      //  //        LORA_state = LORA_IDLE;
      //  ev.commu = FAILSAFE;
      //  //      else
      //  //        LORA_state = LORA_ERROR;
      //}
      //else
      //{
      //  osDelay(1000);

      //  if (loraAppStatus.maxPlayloadSize < loraAppStatus.playloadSize)
      //     ev.commu = FAILSAFE;
      //}
      BSP_LED_On(LED6);
      ev.commu = RUNNING;
#ifdef USE_ULTRASONIC_SENSOR
          loraAppStatus.playloadSize = sizeof(ULTRASONIC_t) * us_sensor.DETECT_OBJECT;
          for(int i = 0; i < us_sensor.DETECT_OBJECT; i++) {
            ULTRASONIC_t *obj = us_sensor.obj + i;
            memcpy(&buf[i*sizeof(ULTRASONIC_t)],
                  (uint8_t *) &obj->distance, sizeof(ULTRASONIC_t));
          }
          fport = 23;
          len = loraAppStatus.playloadSize;
#elif defined ( USE_ADC_SENSOR )
//      do
//      {
//        _event = osMailGet(msgMailHandle, 1000 * 60);
//      }
//      while (_event.status != (osEventMail) );
//      st = _event.value.p;
#if defined ( USE_ADC_SENSOR_4_20MA)
      fport = 25;
#else
      fport = 15;
#endif
      len = sizeof(sData_t);
      memcpy(&buf[0], (uint8_t*) &sData_t, len);

#elif defined( USE_AQI_SENSOR )
      buf[0] = pm_sensor.pm2_5_val >> 8;
      buf[1] = pm_sensor.pm2_5_val >> 0;
      buf[2] = pm_sensor.pm10_val >> 8;
      buf[3] = pm_sensor.pm10_val >> 0;
      buf[4] = co2_sensor.co2_uf_val >> 8;
      buf[5] = co2_sensor.co2_uf_val >> 0;
      buf[6] = co2_sensor.temperature >> 8;
      buf[7] = co2_sensor.temperature >> 0;
      buf[8] = sl_sensor.sound_lvl >> 8;
      buf[9] = sl_sensor.sound_lvl >> 0;
      printf("pm2.5: %d ug/m3; pm10: %d ug/m3 co2: %d ppm; T=%.2f sound=%d dBA\r\n",
                      pm_sensor.pm2_5_val, pm_sensor.pm10_val, co2_sensor.co2_f_val,
                      co2_sensor.temperature / 100.0f, sl_sensor.sound_lvl);
      printf("SEND MSG.: len:%d\r\n", 10);
      fport = 11;
      len = 10;

#endif


#ifdef USE_GPS
      if(gps_data.gprmc_data.xyz.ns == 'S') gps_data.gprmc_data.xyz.lat *= -1.0f;
      if(gps_data.gprmc_data.xyz.ew == 'W') gps_data.gprmc_data.xyz.lon *= -1.0f;

      deg = trunc(gps_data.gprmc_data.xyz.lat / 100.0f);
      minute = gps_data.gprmc_data.xyz.lat - (deg * 100.0f);
      gps_data.gprmc_data.xyz.lat = deg + minute / 60.0f;

      deg = trunc(gps_data.gprmc_data.xyz.lon / 100.0f);
      minute = gps_data.gprmc_data.xyz.lon - (deg * 100.0f);
      gps_data.gprmc_data.xyz.lon = deg + minute / 60.0f;

      tmp =(uint32_t) trunc(gps_data.gprmc_data.xyz.lat * 100000.0f);
      memcpy(&buf[len], (uint8_t*) &tmp , 4);
      printf("lat: %ld ", tmp);
      len += 4;

      tmp = (uint32_t) trunc(gps_data.gprmc_data.xyz.lon * 100000.0f);
      memcpy(&buf[len], (uint8_t*) &tmp , 4);
      printf("lon: %ld\r\n", tmp);
      len += 4;
      
      tmp = (uint32_t) trunc(gps_data.gprmc_data.speed * 100.0f);
      memcpy(&buf[len], (uint8_t*) &tmp , 4);
      printf("course speed: %ld\r\n", tmp);
      len += 4;
      
      fport = fport != 0 ? fport : 9;
#endif /* USE_GPS */

  SendUData(fport, (uint8_t *) &buf, len);
  BSP_LED_Off(LED6);
      memset(buf, 0, sizeof(buf));
      len = 0; 
  
  ev.commu = ALARM;
  //osTimerStart(telemetryTimerHandle, 5000 * loraAppStatus.period);
  /* USER CODE END telemetryCallback */
}

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM6 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM6) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */
  else if (htim->Instance == TIM3) {
#ifdef USE_BEEP
    BEEPER_IRQ();
#endif /* USE_BEEP */
  }
  /* USER CODE END Callback 1 */
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  switch(GPIO_Pin) {
  case GPIO_PIN_0: break;
  case GPIO_PIN_1: break;
  case GPIO_PIN_2: 
#if defined ( USE_MB34 ) && defined ( USE_MB12 )
    IMU_IRQ_Handler(NULL);
#endif
  break;
  case GPIO_PIN_3: break;
  case GPIO_PIN_4: break;
  case GPIO_PIN_5: break;
  case GPIO_PIN_6: break;
  case GPIO_PIN_7: break;
#if defined ( USE_IMU_I2C ) && defined ( USE_MB12 )
    IMU_IRQ_Handler(NULL);
#endif
  case GPIO_PIN_8: break;
  case GPIO_PIN_9: break;
  case GPIO_PIN_10: break;
  case GPIO_PIN_11: break;
  case GPIO_PIN_12: break;
  case GPIO_PIN_13: break;
  case GPIO_PIN_14: break;
  case GPIO_PIN_15: break;

  default: break;
  }
}
/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while (1) {
    BSP_LED_Toggle(LED5);
    HAL_Delay(500);
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  printf("Wrong parameters value: file %s on line %ld\r\n", file, line);
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
