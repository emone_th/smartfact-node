/**
  ******************************************************************************
  * @file    IAM20680.c
  * @author  EmOne Application Team
  * @version V1.0.0
  * @date    28-May-2023
  * @brief   This file provides a set of functions needed to manage the IAM20680
  *          MEMS IMUmeter.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2023 EmOne</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of EmOne nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "IAM20680.h"

/** @addtogroup BSP
  * @{
  */ 

/** @addtogroup Components
  * @{
  */ 

/** @addtogroup IAM20680
  * @brief  This file includes the motion sensor driver for IAM20680 motion sensor 
  *         devices.
  * @{
  */

/** @defgroup IAM20680_Private_TypesDefinitions
  * @{
  */

/**
  * @}
  */

/** @defgroup IAM20680_Private_Defines
  * @{
  */

/**
  * @}
  */

/** @defgroup IAM20680_Private_Macros
  * @{
  */

/**
  * @}
  */ 
  
/** @defgroup IAM20680_Private_Variables
  * @{
  */
IMU_DrvTypeDef imuDrv =
{
  IAM20680_Init,
  IAM20680_DeInit,
  IAM20680_ReadID,
  IAM20680_RebootCmd,
  0,
  IAM20680_Click_IntConfig,
  0,
  0,
  IAM20680_ITStatus,
  0,
  0,
  0,
  IAM20680_ReadIMU,
};

/**
  * @}
  */

/** @defgroup IAM20680_Private_FunctionPrototypes
  * @{
  */
  
/**
  * @}
  */

/** @defgroup IAM20680_Private_Functions
  * @{
  */

/**
  * @brief  Set IAM20680 Initialization.
  * @param  InitStruct: contains mask of different init parameters
  * @retval None
  */
void IAM20680_Init(uint16_t InitStruct)
{
  uint8_t ctrl = 0x00;
  
  /* Configure the low level interface */
  IMU_IO_Init();

  /* Configure MEMS: power mode(ODR) and axes enable */
  ctrl = (uint8_t) (InitStruct);
  
  /* Write value to MEMS REG_PWR_MGMT_1 register */
  IMU_IO_Write(&ctrl, REG_PWR_MGMT_1, 1);
  
  /* Configure MEMS: Output Data Rate Selection output data rate to 100 Hz */
  ctrl = (uint8_t) 0x09;
  
  /* Write value to MEMS CTRL_REG5 register */
  IMU_IO_Write(&ctrl, REG_SAMPLE_RATE_DIV, 1);

  /* Configure MEMS: Full Scale Range Selection set the FSR of the accelerometer to 2g*/
  ctrl = (uint8_t) 0x00;
  
  /* Write value to MEMS REG_ACCEL_CONFIG register */
  IMU_IO_Write(&ctrl, REG_ACCEL_CONFIG, 1);

  /* Configure MEMS: Full Scale Range Selection set the FSR of the gyroscope to 250 dps*/
  ctrl = (uint8_t) 0x00;
  
  /* Write value to MEMS REG_ACCEL_CONFIG register */
  IMU_IO_Write(&ctrl, REG_GYRO_CONFIG, 1);

  /* Configure MEMS: Filter Selection set the corner frequency of the DLPF of the accelerometer to 10.2 Hz */
  ctrl = (uint8_t) 0x05;
  
  /* Write value to MEMS REG_ACCEL_CONFIG register */
  IMU_IO_Write(&ctrl, REG_ACCEL_CONFIG_2, 1);

  /* Configure MEMS: Filter Selection set the corner frequency of the DLPF of the gyroscope to 10 Hz */
  ctrl = (uint8_t) 0x05;
  
  /* Write value to MEMS REG_CONFIG register */
  IMU_IO_Write(&ctrl, REG_CONFIG, 1);

}

/**
  * @brief  IAM20680 De-Initialization.
  * @param  None
  * @retval None.
  */
void IAM20680_DeInit(void)
{
  
}

/**
  * @brief  Read IAM20680 device ID.
  * @param  None
  * @retval The Device ID (two bytes).
  */
uint8_t IAM20680_ReadID(void)
{
  uint8_t tmp = 0;

  /* Configure the low level interface */
  IMU_IO_Init();

  /* Read WHO_AM_I register */
  IMU_IO_Read(&tmp, IAM20680_WHO_AM_I_ADDR, 1);
  
  /* Return the ID */
  return (uint16_t)tmp;
}

/**
  * @brief  Set IAM20680 Interrupt configuration
  * @param  IAM20680_InterruptConfig_TypeDef: pointer to a IAM20680_InterruptConfig_TypeDef 
  *         structure that contains the configuration setting for the IAM20680 Interrupt.
  * @retval None
  */
void IAM20680_InterruptConfig(IAM20680_InterruptConfigTypeDef *IAM20680_IntConfigStruct)
{
  uint8_t ctrl = 0x00;

  ///* Configure Interrupt Selection */                     

  ctrl = (uint8_t) IAM20680_IntConfigStruct->Interrupt_Selection_Enable;
  ///* Write value to MEMS REG_INT_ENABLE register */
  IMU_IO_Write(&ctrl, REG_INT_ENABLE, 1);
  
  ///* Configure Interrupt Selection , Request and Signal */                   
  //ctrl = (uint8_t)(IAM20680_IntConfigStruct->Interrupt_Selection_Enable | \
  //                 IAM20680_IntConfigStruct->Interrupt_Request | \
  //                 IAM20680_IntConfigStruct->Interrupt_Signal);
  
  ///* Write value to MEMS CTRL_REG3 register */
  //IMU_IO_Write(&ctrl, IAM20680_CTRL_REG3_ADDR, 1);
  
  ///* Configure State Machine 1 */                   
  //ctrl = (uint8_t)(IAM20680_IntConfigStruct->State_Machine1_Enable | \
  //                 IAM20680_IntConfigStruct->State_Machine1_Interrupt);
  
  ///* Write value to MEMS CTRL_REG1 register */
  //IMU_IO_Write(&ctrl, IAM20680_CTRL_REG1_ADDR, 1);
  
  ///* Configure State Machine 2 */                   
  //ctrl = (uint8_t)(IAM20680_IntConfigStruct->State_Machine2_Enable | \
  //                 IAM20680_IntConfigStruct->State_Machine2_Interrupt);
  
  ///* Write value to MEMS CTRL_REG2 register */
  //IMU_IO_Write(&ctrl, IAM20680_CTRL_REG2_ADDR, 1);

  IMU_IO_ITConfig();
}

/**
  * @brief  Set IAM20680 for click detection
  * @param  None
  * @retval None
  */
void IAM20680_Click_IntConfig(uint16_t InitStruct)
{
  //uint8_t ctrl = 0x00;
    uint8_t tmpreg = 0x00;
  IAM20680_InterruptConfigTypeDef   IAM20680_InterruptStruct; 

  /* Set IAM20680 Interrupt configuration */
  IAM20680_InterruptStruct.Interrupt_Selection_Enable = InitStruct; //IAM20680_INTERRUPT_2_ENABLE;
  IAM20680_InterruptStruct.Interrupt_Signal = IAM20680_INTERRUPT_SIGNAL_HIGH;
  //IAM20680_InterruptStruct.Interrupt_Open = IAM20680_INTERRUPT_OPEN_OD;
  IAM20680_InterruptStruct.Interrupt_Request = IAM20680_INTERRUPT_REQUEST_LATCHED;
  
  //IAM20680_InterruptStruct.State_Machine1_Enable = IAM20680_SM_DISABLE;
  //IAM20680_InterruptStruct.State_Machine2_Enable = IAM20680_SM_ENABLE;
  //IAM20680_InterruptStruct.State_Machine2_Interrupt = IAM20680_SM_INT1;
  IAM20680_InterruptConfig(&IAM20680_InterruptStruct);
  
  /* Set IAM20680 FIFO configuration */  
  ///* Read REG_CONFIG register */
  IMU_IO_Read(&tmpreg, REG_CONFIG, 1);
  
  ///* Set fifo mode  When set to ‘1’, when the FIFO is full, additional writes will not be written to FIFO.
  //When set to ‘0’, when the FIFO is full, additional writes will be written to the FIFO, replacing the oldest data.*/
  tmpreg &= (uint8_t)~FIFO_MODE;
  ////tmpreg |= FIFO_MODE;
  
  ///* Write value to MEMS REG_CONFIG register */
  IMU_IO_Write(&tmpreg, REG_CONFIG, 1);

  tmpreg = BIT_TEMP_FIFO_EN | BITS_GYRO_FIFO_EN | BIT_ACCEL_FIFO_EN;

  ///* Write value to MEMS REG_CONFIG register */
  IMU_IO_Write(&tmpreg, REG_FIFO_EN, 1);

  /* Read REG_PWR_MGMT_2 register */
  IMU_IO_Read(&tmpreg, REG_PWR_MGMT_2, 1);

  tmpreg |= (BIT_PWR_GYRO_STBY | BIT_PWR_ACCEL_STBY);

 /* Write value to MEMS REG_CONFIG register */
  IMU_IO_Write(&tmpreg, REG_PWR_MGMT_2, 1);

  ///* Set IAM20680 State Machines configuration */
  //ctrl=0x03; 
  //IMU_IO_Write(&ctrl, IAM20680_TIM2_1_L_ADDR,1);
  //ctrl=0xC8; 
  //IMU_IO_Write(&ctrl, IAM20680_TIM1_1_L_ADDR,1);
  //ctrl=0x45; 
  //IMU_IO_Write(&ctrl, IAM20680_THRS2_1_ADDR,1);
  //ctrl=0xFC; 
  //IMU_IO_Write(&ctrl, IAM20680_MASK1_A_ADDR,1);
  //ctrl=0xA1; 
  //IMU_IO_Write(&ctrl, IAM20680_SETT1_ADDR,1);
  //ctrl=0x01; 
  //IMU_IO_Write(&ctrl, IAM20680_PR1_ADDR,1);

  //IMU_IO_Write(&ctrl, IAM20680_SETT2_ADDR,1);
  
  ///* Configure State Machine 2 to detect single click */
  //IMU_IO_Write(&ctrl, IAM20680_ST2_1_ADDR,1);
  //ctrl=0x06; 
  //IMU_IO_Write(&ctrl, IAM20680_ST2_2_ADDR,1);
  //ctrl=0x28; 
  //IMU_IO_Write(&ctrl, IAM20680_ST2_3_ADDR,1);
  //ctrl=0x11; 
  //IMU_IO_Write(&ctrl, IAM20680_ST2_4_ADDR,1);
}

/**
  * @brief  Change the lowpower mode for IAM20680.
  * @param  LowPowerMode: new state for the lowpower mode.
  *   This parameter can be one of the following values:
  *     @arg IAM20680_DATARATE_POWERDOWN: Power down mode
  *     @arg IAM20680_DATARATE_3_125: Normal mode. ODR: 3.125 Hz
  *     @arg IAM20680_DATARATE_6_25: Normal mode. ODR: 6.25 Hz
  *     @arg IAM20680_DATARATE_12_5: Normal mode. ODR: 12.5 Hz
  *     @arg IAM20680_DATARATE_25: Normal mode. ODR: 25 Hz
  *     @arg IAM20680_DATARATE_50: Normal mode. ODR: 50 Hz
  *     @arg IAM20680_DATARATE_100: Normal mode. ODR: 100 Hz
  *     @arg IAM20680_DATARATE_400: Normal mode. ODR: 400 Hz
  *     @arg IAM20680_DATARATE_800: Normal mode. ODR: 800 Hz
  *     @arg IAM20680_DATARATE_1600: Normal mode. ODR: 1600 Hz
  * @retval None
  */
//void IAM20680_ODR_LowpowerCmd(uint8_t ODR_LowPowerMode)
//{
//  uint8_t tmpreg;
  
//  /* Read CTRL_REG4 register */
//  IMU_IO_Read(&tmpreg, IAM20680_CTRL_REG4_ADDR, 1);
  
//  /* Set new low power mode configuration */
//  tmpreg &= (uint8_t)~IAM20680_DATARATE_100;
//  tmpreg |= ODR_LowPowerMode;
  
//  /* Write value to MEMS CTRL_REG4 register */
//  IMU_IO_Write(&tmpreg, IAM20680_CTRL_REG4_ADDR, 1);
//}

///**
//  * @brief  Data Rate command. 
//  * @param  DataRateValue: Data rate value.
//  *   This parameter can be one of the following values:
//  *     @arg IAM20680_DATARATE_3_125: 3.125 Hz output data rate 
//  *     @arg IAM20680_DATARATE_6_25: 6.25 Hz output data rate
//  *     @arg IAM20680_DATARATE_12_5: 12.5  Hz output data rate
//  *     @arg IAM20680_DATARATE_25: 25 Hz output data rate
//  *     @arg IAM20680_DATARATE_50: 50 Hz output data rate 
//  *     @arg IAM20680_DATARATE_100: 100 Hz output data rate
//  *     @arg IAM20680_DATARATE_400: 400 Hz output data rate 
//  *     @arg IAM20680_DATARATE_800: 800 Hz output data rate
//  *     @arg IAM20680_DATARATE_1600: 1600 Hz output data rate
//  * @retval None
//  */
//void IAM20680_DataRateCmd(uint8_t DataRateValue)
//{
//  uint8_t tmpreg;
  
//  /* Read CTRL_REG4 register */
//  IMU_IO_Read(&tmpreg, IAM20680_CTRL_REG4_ADDR, 1);
  
//  /* Set new data rate configuration from 100 to 400Hz */
//  tmpreg &= (uint8_t)~IAM20680_DATARATE_400; 
//  tmpreg |= DataRateValue;
  
//  /* Write value to MEMS CTRL_REG4 register */
//  IMU_IO_Write(&tmpreg, IAM20680_CTRL_REG4_ADDR, 1);
//}

///**
//  * @brief  Change the Full Scale of IAM20680.
//  * @param  FS_value: new full scale value. 
//  *   This parameter can be one of the following values:
//  *     @arg IAM20680_FULLSCALE_2: +-2g
//  *     @arg IAM20680_FULLSCALE_4: +-4g
//  *     @arg IAM20680_FULLSCALE_6: +-6g
//  *     @arg IAM20680_FULLSCALE_8: +-8g
//  *     @arg IAM20680_FULLSCALE_16: +-16g
//  * @retval None
//  */
//void IAM20680_FullScaleCmd(uint8_t FS_value)
//{
//  uint8_t tmpreg;
  
//  /* Read CTRL_REG5 register */
//  IMU_IO_Read(&tmpreg, IAM20680_CTRL_REG5_ADDR, 1);
  
//  /* Set new full scale configuration */
//  tmpreg &= (uint8_t)~IAM20680_FULLSCALE_16;
//  tmpreg |= FS_value;
  
//  /* Write value to MEMS CTRL_REG5 register */
//  IMU_IO_Write(&tmpreg, IAM20680_CTRL_REG5_ADDR, 1);
//}

/**
  * @brief  Reboot memory content of IAM20680.
  * @param  None
  * @retval None
  */
void IAM20680_RebootCmd(void)
{
  uint8_t tmpreg;
  /* Read CTRL_REG6 register */
  IMU_IO_Read(&tmpreg, REG_PWR_MGMT_1, 1);
  
  /* Enable or Disable the reboot memory */
  tmpreg |= BIT_H_RESET;
  
  /* Write value to MEMS CTRL_REG6 register */
  IMU_IO_Write(&tmpreg, REG_PWR_MGMT_1, 1);
}

/**
  * @brief  Read IAM20680 output register, and calculate the acceleration 
  *         ACC[mg]=SENSITIVITY* (out_h*256+out_l)/16 (12 bit representation).
  * @param  pointer on floating buffer.
  * @retval None
  */
void IAM20680_ReadIMU(int16_t *pData)
{
  int8_t buffer[14] = { 0x00 };
  uint8_t crtl, i = 0x00;
  float sensitivity = IAM20680_SENSITIVITY_0_06G;
  float dps_rate = IAM20680_GYRO_SENSITIVITY_250;
  float valueinfloat = 0;
  
  IMU_IO_Read((uint8_t*)&buffer[0], REG_RAW_ACCEL, 6);
  IMU_IO_Read((uint8_t*)&buffer[6], REG_RAW_TEMP, 2);
  IMU_IO_Read((uint8_t*)&buffer[8], REG_RAW_GYRO, 6);
  
  IMU_IO_Read(&crtl, REG_ACCEL_CONFIG, 1); 
  switch((crtl >> SHIFT_ACCEL_FS) & IAM20680__FULLSCALE_SELECTION) 
  {
    /* FS bit = 000 ==> Sensitivity typical value = 0.06milligals/digit */ 
  case IAM20680_FULLSCALE_2:
    sensitivity = IAM20680_SENSITIVITY_0_06G;
    break;
    
    /* FS bit = 001 ==> Sensitivity typical value = 0.12milligals/digit */ 
  case IAM20680_FULLSCALE_4:
    sensitivity = IAM20680_SENSITIVITY_0_12G;
    break;
    
    /* FS bit = 010 ==> Sensitivity typical value = 0.18milligals/digit */ 
  //case IAM20680_FULLSCALE_6:
  //  sensitivity = IAM20680_SENSITIVITY_0_18G;
  //  break;
    
    /* FS bit = 011 ==> Sensitivity typical value = 0.24milligals/digit */ 
  case IAM20680_FULLSCALE_8:
    sensitivity = IAM20680_SENSITIVITY_0_24G;
    break;
    
    /* FS bit = 100 ==> Sensitivity typical value = 0.73milligals/digit */ 
  case IAM20680_FULLSCALE_16:
    sensitivity = IAM20680_SENSITIVITY_0_73G;
    break;
    
  default:
    break;
  }
  
  IMU_IO_Read(&crtl, REG_GYRO_CONFIG, 1); 
  switch((crtl >> SHIFT_GYRO_FS_SEL) & IAM20680_GYRO_FULLSCALE_SELECTION) 
  {
    /* FS bit = 000 ==> Sensitivity typical value = 0.004 dps/digit */ 
  case IAM20680_GYRO_FULLSCALE_250:
    dps_rate = IAM20680_GYRO_SENSITIVITY_250;
    break;
    
    /* FS bit = 001 ==> Sensitivity typical value = 0.002 dps/digit */ 
  case IAM20680_GYRO_FULLSCALE_500:
    dps_rate = IAM20680_GYRO_SENSITIVITY_500;
    break;
    
    /* FS bit = 011 ==> Sensitivity typical value = 0.0.001 dps/digit */ 
  case IAM20680_GYRO_FULLSCALE_1000:
    dps_rate = IAM20680_GYRO_SENSITIVITY_1000;
    break;
    
    /* FS bit = 100 ==> Sensitivity typical value = 0.0.0005 dps/digit */ 
  case IAM20680_GYRO_FULLSCALE_2000:
    dps_rate = IAM20680_GYRO_SENSITIVITY_2000;
    break;
    
  default:
    break;
  }

  /* Obtain the mg value for the three axis */
  for(i=0; i<7; i++)
  {
    valueinfloat = ((buffer[2*i] << 8) + buffer[2*i+1]); //
    if(i<3) {
      valueinfloat *= sensitivity; //Accel
    }
    else if (i==3) { //Temperature
      valueinfloat *= 1.0f;
    }
    else if (i>3) {
      valueinfloat *= dps_rate; // Gyro
    }
    pData[i] = (int16_t)valueinfloat;
  }

}

/**
  * @}
  */ 
uint8_t IAM20680_ITStatus(void *handler) {
  uint8_t tmpreg = 0x00;
  
  /* Read REG_CONFIG register */
  IMU_IO_Read(&tmpreg, REG_INT_STATUS, 1);

  return tmpreg;
}
/**
  * @}
  */ 
uint16_t IAM20680_FIFO_COUNT(uint16_t *pData)
{
  uint8_t tmpreg[2];
  
  /* Read REG_CONFIG register */
  IMU_IO_Read(&tmpreg, REG_FIFO_COUNT_H, FIFO_COUNT_BYTE);

  *pData = tmpreg[0] << 8 + tmpreg[1];

  return *pData;    
}
/**
  * @}
  */ 

/**
  * @}
  */ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
