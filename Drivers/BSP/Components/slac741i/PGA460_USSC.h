/*
	PGA460_USSC.h

	BSD 2-clause "Simplified" License
	Copyright (c) 2017, Texas Instruments
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright notice, this
	   list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright notice,
	   this list of conditions and the following disclaimer in the documentation
	   and/or other materials provided with the distribution.

	THIS SOFTWARE IS PROVIDED BY TEXAS INSTRUMENTS "AS IS" AND
	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL TEXAS INSTRUMENTS BE LIABLE FOR
	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

	The views and conclusions contained in the software and documentation are those
	of the authors and should not be interpreted as representing official policies,
	either expressed or implied, of the FreeBSD Project.

	Last Updated: Nov 2017
	By: A. Whitehead <make@energia.nu>

	Maintain by : Anol P. <anol.p@emone.co.th> 2020
*/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

typedef uint8_t byte;

byte pga460_pullEchoDataDump(byte element);
byte pga460_registerRead(byte addr);
byte pga460_registerWrite(byte addr, byte data);
void pga460_initBoostXLPGA460(byte mode, uint32_t baud, byte uartAddrUpdate);
void pga460_defaultPGA460(byte xdcr);
void pga460_initThresholds(byte thr);
void pga460_initTVG(byte agr, byte tvg);
void pga460_ultrasonicCmd(byte cmd, byte numObjUpdate);
void pga460_runEchoDataDump(byte preset);
void pga460_broadcast(bool eeBulk, bool tvgBulk, bool thrBulk);
void pga460_toggleLEDs(bool ds1State, bool fdiagState, bool vdiagState);
void pga460_autoThreshold(byte cmd, byte noiseMargin, byte windowIndex, byte autoMax, byte avgLoops);
void pga460_eepromThreshold(byte preset, bool saveLoad);
void pga460_thresholdBulkRead(byte preset);
void pga460_thresholdBulkWrite(byte p1ThrMap[], byte p2ThrMap[]);
bool pga460_burnEEPROM( void );
bool pga460_pullUltrasonicMeasResult(bool busDemo);
double pga460_printUltrasonicMeasResult(byte umr, float temperature);
byte pga460_printUltrasonicMeasResultRaw(byte umr);
double pga460_printUltrasonicMeasResultExt(byte umr, int speedSound);
double pga460_runDiagnostics(byte run, byte diag);
double pga460_triangulation(double a, double b, double c);
void pga460_pullEchoDataDumpBulk( void );

static byte pga460_calcChecksum(byte cmd);
static void pga460_SerialFlush( void );
//static void pga460_tciRecord(byte numObj);
//static void pga460_tciByteToggle(byte data, byte zeroPadding);
//static void pga460_tciIndexRW(byte index, bool write);
//static void pga460_tciCommand(byte cmd);
//static void pga460_spiTransfer(byte* mosi, byte size);
//static void pga460_spiMosiIdle(byte size);

int pga460_sndSerialData(uint8_t *data, size_t len);
int pga460_rcvSerialData(uint8_t *data, size_t len);