/**
 ******************************************************************************
 * @file    stm32_bus_ex.c
 * @author  MCD Application Team
 * @brief   Source file for GNSS1A1 Bus Extension
 ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_nucleo_bus.h"
#include "stm32_bus_ex.h"

/**
  * @brief  Transmit data to the device through BUS.
  * @param  DevAddr Device address on Bus (I2C only).
  * @param  pData  Pointer to data buffer to write
  * @param  Length Data Length
  * @retval BSP status
  */
int32_t BSP_USART3_Send_IT(uint16_t DevAddr, uint8_t *pData, uint16_t Length)
{
  int32_t ret = BSP_ERROR_BUS_FAILURE;

  UNUSED(DevAddr);

  if(HAL_UART_Transmit_IT(&huart3, (uint8_t *)pData, Length) == HAL_OK)
  {
    ret = BSP_ERROR_NONE;
  }
  else
  {
    ret =  BSP_ERROR_PERIPH_FAILURE;
  }
  return ret;
}

/**
  * @brief  Receive data from the device through BUS.
  * @param  DevAddr Device address on Bus (I2C only).
  * @param  pData Pointer to data buffer to write
  * @param  Length Data Length
  * @retval BSP status
  */
int32_t BSP_USART3_Recv_IT(uint16_t DevAddr, uint8_t *pData, uint16_t Length)
{
  int32_t ret = BSP_ERROR_BUS_FAILURE;

  UNUSED(DevAddr);

  if(HAL_UART_Receive_IT(&huart3, (uint8_t *)pData, Length) == HAL_OK)
  {
    ret = BSP_ERROR_NONE;
  }
  else
  {
    ret =  BSP_ERROR_PERIPH_FAILURE;
  }
  return ret;
}

#if (USE_HAL_UART_REGISTER_CALLBACKS == 1)
int32_t BSP_USART3_RegisterRxCallback(pUART_CallbackTypeDef pCallback)
{
  return HAL_UART_RegisterCallback(&huart3, HAL_UART_RX_COMPLETE_CB_ID, pCallback);
}

int32_t BSP_USART3_RegisterErrorCallback(pUART_CallbackTypeDef pCallback)
{
  return HAL_UART_RegisterCallback(&huart3, HAL_UART_ERROR_CB_ID, pCallback);
}
#endif

void BSP_USART3_ClearOREF(void)
{
  __HAL_UART_CLEAR_FLAG(&huart3, UART_FLAG_ORE);
}

void BSP_USART3_IRQHanlder(void)
{
  HAL_UART_IRQHandler(&huart3);
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
